import 'package:flutter/material.dart';
import 'package:poc_ng/screens/index.dart';
import 'package:poc_ng/screens/welcome/welcome_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthController {
  String _welcome;

  String get welcome => _welcome;

  void setAuth(BuildContext context, String welcome) async {
    if (welcome != null) {
      _welcome = welcome;

      await saveSession(welcome);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) {
        return IndexPages();
      }));
    } else {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) {
        return WelcomeScreen();
      }));
    }
  }

  Future<void> saveSession(String welcome) async {
    final prefs = await SharedPreferences.getInstance();

    await prefs.setString('WELCOME_SCREEN', 'true');
    return;
  }

  Future<void> currentSession(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();

    await Future.delayed(const Duration(seconds: 2));

    if (prefs.containsKey('WELCOME_SCREEN')) {
      final welcome = prefs.getString('WELCOME_SCREEN');
      setAuth(context, welcome);
      return;
    } else {
      setAuth(context, null);
    }
  }
}
