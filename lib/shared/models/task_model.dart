class TaskModel {
  final String id;
  final String description;

  TaskModel({this.id, this.description});

  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
      'description': this.description ?? null,
    };
  }

  factory TaskModel.fromJson(Map<String, dynamic> equipament) {
    return TaskModel(
      id: equipament['id'],
      description: equipament['description'],
    );
  }
}
