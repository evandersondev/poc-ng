class EquipamentModel {
  final String id;
  final String code;
  final String description;
  final String localization;

  EquipamentModel({this.id, this.code, this.description, this.localization});

  Map<String, dynamic> toJson() {
    return {
      'id': this?.id ?? '',
      'code': this?.code ?? '',
      'description': this?.description ?? '',
      'localization': this?.localization ?? '',
    };
  }

  factory EquipamentModel.fromJson(Map<String, dynamic> equipament) {
    return EquipamentModel(
      id: equipament['id'] ?? '',
      code: equipament['code'] ?? '',
      description: equipament['description'] ?? '',
      localization: equipament['localization'] ?? '',
    );
  }
}
