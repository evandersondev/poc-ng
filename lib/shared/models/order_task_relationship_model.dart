import 'package:poc_ng/shared/models/task_model.dart';

import 'service_order_model.dart';

class OrderTaskRelationshipModel {
  final String id;
  final ServiceOrderModel serviceOrder;
  final TaskModel task;
  final bool checked;

  OrderTaskRelationshipModel({
    this.id,
    this.serviceOrder,
    this.task,
    this.checked,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
      'serviceOrder': this?.serviceOrder?.toJson(),
      'task': this.task.toJson(),
      'checked': this.checked ?? false
    };
  }

  factory OrderTaskRelationshipModel.fromJson(Map<String, dynamic> json) {
    return OrderTaskRelationshipModel(
      id: json['id'],
      serviceOrder: ServiceOrderModel?.fromJson(json['serviceOrder']) ?? null,
      task: TaskModel.fromJson(json['task']) ?? null,
      checked: json['checked'] ?? false,
    );
  }
}
