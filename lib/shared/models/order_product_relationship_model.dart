import 'package:poc_ng/shared/models/product_model.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';

class OrderProductRelationshipModel {
  final String id;
  final ServiceOrderModel serviceOrder;
  final ProductModel product;
  final int productAmount;
  final bool checked;

  OrderProductRelationshipModel(
      {this.id,
      this.serviceOrder,
      this.product,
      this.productAmount,
      this.checked});

  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
      'serviceOrder': this?.serviceOrder?.toJson() ?? null,
      'product': this?.product?.toJson() ?? null,
      'productAmount': this?.productAmount ?? null,
      'checked': this?.checked ?? false,
    };
  }

  factory OrderProductRelationshipModel.fromJson(Map<String, dynamic> json) {
    return OrderProductRelationshipModel(
        id: json['id'],
        serviceOrder: ServiceOrderModel?.fromJson(json['serviceOrder'] ?? null),
        product: ProductModel.fromJson(json['product']) ?? null,
        productAmount: json['productAmount'],
        checked: json['checked'] ?? false);
  }
}
