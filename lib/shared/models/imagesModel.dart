import 'dart:io';

class ImagesModel {
  final File image;
  final String serviceOrderId;

  ImagesModel({this.image, this.serviceOrderId});

  Map<String, dynamic> toJson() {
    return {
      'image': this.image,
      'serviceOrderId': this.serviceOrderId,
    };
  }

  factory ImagesModel.fromJson(Map<String, dynamic> json) {
    return ImagesModel(
      image: File(json['image']) ?? null,
      serviceOrderId: json['serviceOrderId'] ?? '',
    );
  }
}
