import 'equipament_model.dart';

enum StatusEnum { open, closed, canceled }

Map<StatusEnum, String> statusMap = {
  StatusEnum.open: 'Aberta',
  StatusEnum.closed: 'Finalizada',
  StatusEnum.canceled: 'Cancelada',
};

class ServiceOrderModel {
  final String id;
  final String code;
  final String description;
  final String localization;
  final EquipamentModel equipament;
  final String expectedStart;
  final String doneDate;
  final String status;
  final String details;
  final String reason;
  final String report;
  final String createdAt;
  final String updatedAt;

  ServiceOrderModel({
    this.id,
    this.code,
    this.expectedStart,
    this.doneDate,
    this.description,
    this.localization,
    this.equipament,
    this.status,
    this.details,
    this.reason,
    this.report,
    this.createdAt,
    this.updatedAt,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
      'code': this.code ?? '',
      'description': this?.description ?? '',
      'localization': this?.localization ?? '',
      'equipament': this.equipament != null ? this.equipament.toJson() : null,
      'expectedStart': this.expectedStart ?? '',
      'doneDate': this.doneDate ?? '',
      'status': this.status ?? '',
      'details': this.details ?? '',
      'reason': this.reason ?? '',
      'report': this.report ?? '',
      'createdAt': this.createdAt ?? '',
      'updatedAt': this.updatedAt ?? '',
    };
  }

  factory ServiceOrderModel.fromJson(Map<String, dynamic> json) {
    return ServiceOrderModel(
        id: json['id'] ?? '',
        code: json['code'] ?? '',
        description: json['description'] ?? '',
        localization: json['localization'] ?? '',
        equipament: json['equipament'] != null
            ? EquipamentModel?.fromJson(json['equipament'])
            : null,
        expectedStart: json['expectedStart'] ?? '',
        doneDate: json['doneDate'] ?? '',
        status: json['status'] ?? statusMap[StatusEnum.open],
        details: json['details'] ?? '',
        reason: json['reason'] ?? '',
        report: json['report'] ?? '',
        createdAt: json['createdAt'] ?? '',
        updatedAt: json['updatedAt'] ?? '');
  }
}
