class ProductModel {
  final String id;
  final String description;

  ProductModel({this.id, this.description});

  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
      'description': this.description,
    };
  }

  factory ProductModel.fromJson(Map<String, dynamic> equipament) {
    return ProductModel(
      id: equipament['id'],
      description: equipament['description'],
    );
  }
}
