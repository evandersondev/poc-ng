import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppThemeController extends ChangeNotifier {
  bool isDark = false;

  changeTheme() async {
    final prefs = await SharedPreferences.getInstance();
    isDark = !isDark;
    if (isDark) {
      prefs.setString('app_theme', 'Dark');
    } else {
      prefs.setString('app_theme', 'Ligth');
    }

    notifyListeners();
  }

  static AppThemeController instance = AppThemeController();
}
