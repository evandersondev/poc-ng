import 'package:shared_preferences/shared_preferences.dart';

Future<void> setLocalStorage({String key, String value}) async {
  final prefs = await SharedPreferences.getInstance();

  prefs.setString(key, value);
}
