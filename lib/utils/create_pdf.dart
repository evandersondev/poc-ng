import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pdfLib;
import 'package:path_provider/path_provider.dart';
import 'package:poc_ng/assets/images.dart';
import 'package:poc_ng/shared/models/order_product_relationship_model.dart';
import 'package:poc_ng/shared/models/order_task_relationship_model.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';

class CreatePdf {
  static final CreatePdf pdf = CreatePdf();

  Future<String> view(
      ServiceOrderModel order,
      List<OrderProductRelationshipModel> products,
      List<OrderTaskRelationshipModel> tasks) async {
    final pdf = pdfLib.Document(deflate: zlib.encode);

    pdfLib.PageTheme buildTheme() {
      return pdfLib.PageTheme(
        margin: pdfLib.EdgeInsets.all(30),
        pageFormat: PdfPageFormat.a4,
        buildBackground: (context) => pdfLib.FullPage(
          ignoreMargins: true,
        ),
      );
    }

    final logo = (await rootBundle.load(logoName)).buffer.asUint8List();

    pdfLib.Widget showObservation() {
      if (order.status == 'closed' && order.details != '' ??
          order.details != null) {
        return pdfLib.Column(
            crossAxisAlignment: pdfLib.CrossAxisAlignment.start,
            children: [
              pdfLib.SizedBox(height: 10),
              pdfLib.Text(order.details),
              pdfLib.Divider(thickness: 0.5, height: 1),
              pdfLib.SizedBox(height: 8),
              pdfLib.Divider(thickness: 0.5, height: 20),
              pdfLib.Divider(thickness: 0.5, height: 20),
              pdfLib.Divider(thickness: 0.5, height: 20),
            ]);
      } else if (order.status == 'canceled' && order.reason != '' ??
          order.reason != null) {
        return pdfLib.Column(
            crossAxisAlignment: pdfLib.CrossAxisAlignment.start,
            children: [
              pdfLib.SizedBox(height: 10),
              pdfLib.Text(order.reason),
              pdfLib.Divider(thickness: 0.5, height: 1),
              pdfLib.SizedBox(height: 8),
              pdfLib.Divider(thickness: 0.5, height: 20),
              pdfLib.Divider(thickness: 0.5, height: 20),
              pdfLib.Divider(thickness: 0.5, height: 20),
            ]);
      } else {
        return pdfLib.Column(
            crossAxisAlignment: pdfLib.CrossAxisAlignment.start,
            children: [
              pdfLib.SizedBox(height: 10),
              pdfLib.Divider(thickness: 0.5, height: 20),
              pdfLib.Divider(thickness: 0.5, height: 20),
              pdfLib.Divider(thickness: 0.5, height: 20),
              pdfLib.Divider(thickness: 0.5, height: 20),
            ]);
      }
    }

    pdf.addPage(pdfLib.MultiPage(
      pageTheme: buildTheme(),
      header: (pdfLib.Context context) => pdfLib.Column(
        children: [
          pdfLib.Row(
            mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
            children: [
              pdfLib.Text(
                'ORDEM DE SERVIÇO N° ${order.code}',
                textAlign: pdfLib.TextAlign.center,
                style: pdfLib.TextStyle(
                    fontWeight: pdfLib.FontWeight.bold, fontSize: 16),
              ),
              pdfLib.Column(children: [
                pdfLib.Image(pdfLib.MemoryImage(logo),
                    height: 30, fit: pdfLib.BoxFit.contain),
              ]),
            ],
          ),
          pdfLib.Divider(height: 32),
          pdfLib.SizedBox(height: 30),
        ],
      ),
      footer: (pdfLib.Context context) => pdfLib.Column(
        children: [
          pdfLib.Divider(height: 32),
          pdfLib.Row(
            children: [
              pdfLib.Text(
                'ORDEM DE SERVIÇO N° ${order.code}',
                textAlign: pdfLib.TextAlign.center,
                style: pdfLib.TextStyle(
                    fontWeight: pdfLib.FontWeight.bold, fontSize: 16),
              ),
            ],
          ),
        ],
      ),
      build: (context) => [
        pdfLib.Row(
            mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
            crossAxisAlignment: pdfLib.CrossAxisAlignment.start,
            children: [
              pdfLib.Column(
                  crossAxisAlignment: pdfLib.CrossAxisAlignment.start,
                  children: [
                    pdfLib.Row(children: [
                      pdfLib.Text('Relato do problema: ',
                          style: pdfLib.TextStyle(
                              fontWeight: pdfLib.FontWeight.bold)),
                      pdfLib.Text(order?.description ?? 'Não informado')
                    ]),
                    pdfLib.SizedBox(height: 2),
                    pdfLib.Row(children: [
                      pdfLib.Text('Equipamento: ',
                          style: pdfLib.TextStyle(
                              fontWeight: pdfLib.FontWeight.bold)),
                      pdfLib.Text(
                          order?.equipament?.description ?? 'Não informado')
                    ]),
                    pdfLib.SizedBox(height: 2),
                    order.localization != ''
                        ? pdfLib.Row(children: [
                            pdfLib.Text('Localização: ',
                                style: pdfLib.TextStyle(
                                    fontWeight: pdfLib.FontWeight.bold)),
                            pdfLib.Text(order.localization)
                          ])
                        : pdfLib.Container(),
                    pdfLib.Column(
                        crossAxisAlignment: pdfLib.CrossAxisAlignment.start,
                        children: [
                          pdfLib.SizedBox(height: 2),
                          order.status == 'closed'
                              ? pdfLib.Row(children: [
                                  pdfLib.Text('Data de Previsão:',
                                      style: pdfLib.TextStyle(
                                          fontWeight: pdfLib.FontWeight.bold)),
                                  pdfLib.Text(
                                      '${DateFormat(' dd/ MM/ yyyy  hh:mm', 'pt-BR').format(DateTime.parse(order?.expectedStart))}'),
                                  pdfLib.SizedBox(width: 30),
                                  pdfLib.Text('Data de Realização:',
                                      style: pdfLib.TextStyle(
                                          fontWeight: pdfLib.FontWeight.bold)),
                                  pdfLib.Text(
                                      '${DateFormat(' dd/ MM/ yyyy  hh:mm', 'pt-BR').format(DateTime.parse(order?.doneDate))}'),
                                ])
                              : pdfLib.Row(children: [
                                  pdfLib.Text('Data de Previsão:',
                                      style: pdfLib.TextStyle(
                                          fontWeight: pdfLib.FontWeight.bold)),
                                  pdfLib.Text(
                                      '${DateFormat(' dd/ MM/ yyyy  hh:mm', 'pt-BR').format(DateTime.parse(order?.expectedStart))}'),
                                  pdfLib.SizedBox(width: 30),
                                  pdfLib.Text('Data de Realização:',
                                      style: pdfLib.TextStyle(
                                          fontWeight: pdfLib.FontWeight.bold)),
                                  pdfLib.Text(' ____/____/____  ____:____'),
                                ]),
                        ]),
                    pdfLib.SizedBox(height: 30),
                  ]),
            ]),
        pdfLib.Column(
            crossAxisAlignment: pdfLib.CrossAxisAlignment.start,
            mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
            children: [
              // ? Start list products and tasks
              order.status == 'closed'
                  ? pdfLib.Column(
                      crossAxisAlignment: pdfLib.CrossAxisAlignment.start,
                      children: [
                          pdfLib.Row(
                              crossAxisAlignment: pdfLib.CrossAxisAlignment.end,
                              mainAxisAlignment:
                                  pdfLib.MainAxisAlignment.center,
                              children: [
                                pdfLib.Expanded(
                                  flex: 3,
                                  child: pdfLib.Column(
                                    crossAxisAlignment:
                                        pdfLib.CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        pdfLib.MainAxisAlignment.end,
                                    children: [
                                      pdfLib.SizedBox(height: 20),
                                      pdfLib.Text('Recursos',
                                          style: pdfLib.TextStyle(
                                              fontWeight:
                                                  pdfLib.FontWeight.bold)),
                                      ...products
                                          .where((element) =>
                                              element.checked == true)
                                          .map((item) {
                                        return pdfLib.Column(
                                            crossAxisAlignment:
                                                pdfLib.CrossAxisAlignment.start,
                                            children: [
                                              pdfLib.Text(
                                                  item.product.description),
                                              pdfLib.Divider(
                                                  thickness: 0.5, height: 1),
                                              pdfLib.SizedBox(height: 2.5),
                                            ]);
                                      }).toList(),
                                      pdfLib.SizedBox(height: 6),
                                      pdfLib.Divider(
                                          thickness: 0.5, height: 20),
                                      pdfLib.Divider(
                                          thickness: 0.5, height: 20),
                                      pdfLib.Divider(
                                          thickness: 0.5, height: 20),
                                      pdfLib.Divider(
                                          thickness: 0.5, height: 20),
                                      pdfLib.Divider(
                                          thickness: 0.5, height: 20),
                                    ],
                                  ),
                                ),
                                pdfLib.SizedBox(width: 30),
                                pdfLib.Expanded(
                                  flex: 1,
                                  child: pdfLib.Column(
                                      mainAxisAlignment:
                                          pdfLib.MainAxisAlignment.end,
                                      children: [
                                        pdfLib.Text('Quantidade',
                                            style: pdfLib.TextStyle(
                                                fontWeight:
                                                    pdfLib.FontWeight.bold)),
                                        ...products
                                            .where((element) =>
                                                element.checked == true)
                                            .map((item) {
                                          return pdfLib.Column(
                                              crossAxisAlignment: pdfLib
                                                  .CrossAxisAlignment.center,
                                              children: [
                                                pdfLib.Text(item.productAmount
                                                    .toString()),
                                                pdfLib.Divider(
                                                    thickness: 0.5, height: 1),
                                              ]);
                                        }).toList(),
                                        pdfLib.SizedBox(height: 8),
                                        pdfLib.Divider(
                                            thickness: 0.5, height: 20),
                                        pdfLib.Divider(
                                            thickness: 0.5, height: 20),
                                        pdfLib.Divider(
                                            thickness: 0.5, height: 20),
                                        pdfLib.Divider(
                                            thickness: 0.5, height: 20),
                                        pdfLib.Divider(
                                            thickness: 0.5, height: 20),
                                      ]),
                                ),
                              ]),
                          pdfLib.Column(
                              crossAxisAlignment:
                                  pdfLib.CrossAxisAlignment.start,
                              children: [
                                pdfLib.Text('Tarefas',
                                    style: pdfLib.TextStyle(
                                        fontWeight: pdfLib.FontWeight.bold)),
                                pdfLib.SizedBox(height: 10),
                                ...tasks.map((item) {
                                  return pdfLib.Column(
                                      crossAxisAlignment:
                                          pdfLib.CrossAxisAlignment.start,
                                      children: [
                                        pdfLib.Text(item.task.description),
                                        pdfLib.Divider(
                                            thickness: 0.5, height: 1),
                                        pdfLib.SizedBox(height: 2.5),
                                      ]);
                                }).toList(),
                                pdfLib.SizedBox(height: 8),
                                pdfLib.Divider(thickness: 0.5, height: 20),
                                pdfLib.Divider(thickness: 0.5, height: 20),
                                pdfLib.Divider(thickness: 0.5, height: 20),
                                pdfLib.Divider(thickness: 0.5, height: 20),
                              ]),
                        ])

                  // ? Start list products and tasks
                  : pdfLib.Column(
                      crossAxisAlignment: pdfLib.CrossAxisAlignment.start,
                      children: [
                          pdfLib.Container(
                            height: 150,
                            width: PdfPageFormat.a4.width,
                            child: pdfLib.Row(
                                crossAxisAlignment:
                                    pdfLib.CrossAxisAlignment.center,
                                children: [
                                  pdfLib.Container(
                                    height: 150,
                                    width: PdfPageFormat.a4.width - 210,
                                    child: pdfLib.Expanded(
                                      flex: 3,
                                      child: pdfLib.Column(
                                        mainAxisAlignment:
                                            pdfLib.MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            pdfLib.CrossAxisAlignment.start,
                                        children: [
                                          pdfLib.Text('Recursos',
                                              style: pdfLib.TextStyle(
                                                  fontWeight:
                                                      pdfLib.FontWeight.bold)),
                                          pdfLib.SizedBox(height: 10),
                                          ...products.map((item) {
                                            return pdfLib.Column(
                                                crossAxisAlignment: pdfLib
                                                    .CrossAxisAlignment.start,
                                                children: [
                                                  pdfLib.Text(
                                                      item.product.description),
                                                  pdfLib.Divider(
                                                      thickness: 0.5,
                                                      height: 1),
                                                  pdfLib.SizedBox(height: 2.5),
                                                ]);
                                          }).toList(),
                                          pdfLib.SizedBox(height: 6),
                                          pdfLib.Divider(
                                              thickness: 0.5, height: 20),
                                          pdfLib.Divider(
                                              thickness: 0.5, height: 20),
                                          pdfLib.Divider(
                                              thickness: 0.5, height: 20),
                                        ],
                                      ),
                                    ),
                                  ),
                                  pdfLib.SizedBox(width: 30),
                                  pdfLib.Container(
                                    height: 150,
                                    width: 120,
                                    child: pdfLib.Expanded(
                                      flex: 1,
                                      child: pdfLib.Column(
                                          mainAxisAlignment:
                                              pdfLib.MainAxisAlignment.start,
                                          children: [
                                            pdfLib.Text('Quantidade',
                                                style: pdfLib.TextStyle(
                                                    fontWeight: pdfLib
                                                        .FontWeight.bold)),
                                            pdfLib.SizedBox(height: 10),
                                            ...products.map((item) {
                                              return pdfLib.Column(
                                                  crossAxisAlignment: pdfLib
                                                      .CrossAxisAlignment
                                                      .center,
                                                  children: [
                                                    pdfLib.Text(item
                                                        .productAmount
                                                        .toString()),
                                                    pdfLib.Divider(
                                                        thickness: 0.5,
                                                        height: 1),
                                                    pdfLib.SizedBox(
                                                        height: 2.5),
                                                  ]);
                                            }).toList(),
                                            pdfLib.SizedBox(height: 8),
                                            pdfLib.Divider(
                                                thickness: 0.5, height: 19),
                                            pdfLib.Divider(
                                                thickness: 0.5, height: 19),
                                            pdfLib.Divider(
                                                thickness: 0.5, height: 19),
                                          ]),
                                    ),
                                  ),
                                ]),
                          ),
                          pdfLib.SizedBox(height: 10),
                          pdfLib.Column(
                              crossAxisAlignment:
                                  pdfLib.CrossAxisAlignment.start,
                              children: [
                                pdfLib.Text('Tarefas',
                                    style: pdfLib.TextStyle(
                                        fontWeight: pdfLib.FontWeight.bold)),
                                pdfLib.SizedBox(height: 10),
                                ...tasks.map((item) {
                                  return pdfLib.Column(
                                      crossAxisAlignment:
                                          pdfLib.CrossAxisAlignment.start,
                                      children: [
                                        pdfLib.Text(item.task.description),
                                        pdfLib.Divider(
                                            thickness: 0.5, height: 1),
                                        pdfLib.SizedBox(height: 2.5),
                                      ]);
                                }).toList(),
                                pdfLib.SizedBox(height: 8),
                                pdfLib.Divider(thickness: 0.5, height: 20),
                                pdfLib.Divider(thickness: 0.5, height: 20),
                                pdfLib.Divider(thickness: 0.5, height: 20),
                              ]),
                        ]),
            ]),
        pdfLib.SizedBox(height: 10),
        pdfLib.SizedBox(height: 30),
        pdfLib.Column(
            crossAxisAlignment: pdfLib.CrossAxisAlignment.start,
            children: [
              pdfLib.Text('Observações',
                  style: pdfLib.TextStyle(fontWeight: pdfLib.FontWeight.bold)),
              showObservation(),
            ]),
        pdfLib.SizedBox(height: 30),
      ],
    ));

    final directory = (await getApplicationDocumentsDirectory()).path;
    final path = '$directory/${order.code}.pdf';
    final file = File(path);
    file.writeAsBytesSync(await pdf.save());

    return path;
  }
}
