import 'package:flutter/material.dart';
import 'package:poc_ng/assets/images.dart';
import 'package:poc_ng/shared/auth/auth_controller.dart';
import 'package:poc_ng/shared/themes/app_colors.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authController = AuthController();
    authController.currentSession(context);

    return Scaffold(
      backgroundColor: AppColors.primary,
      body: Container(
        child: Center(
          child: Container(
              height: 200,
              child: Image.asset(
                logoTipo,
                fit: BoxFit.contain,
                filterQuality: FilterQuality.high,
              )),
        ),
      ),
    );
  }
}
