import 'package:flutter/material.dart';
import 'package:poc_ng/screens/home/home_screen.dart';
import 'package:poc_ng/screens/order/list/order_list_screen.dart';
import 'package:poc_ng/screens/schedule/schedule.dart';
import 'package:poc_ng/screens/widgets/navgation_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IndexPages extends StatefulWidget {
  final int pageIndex;

  const IndexPages({this.pageIndex});
  @override
  _IndexPagesState createState() => _IndexPagesState();
}

class _IndexPagesState extends State<IndexPages> {
  int currentIndex = 0;

  void changePage(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  final tabs = [
    HomeScreen(),
    OrderListScreen(),
    ScheduleScreen(),
  ];

  Future<void> saveWelcomeScreen() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('WELCOME_SCREEN', 'true');
  }

  void initAsync() async {
    await saveWelcomeScreen();
  }

  void initState() {
    super.initState();
    initAsync();

    if (widget.pageIndex != null) {
      setState(() {
        currentIndex = widget.pageIndex;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: NavigationBar(
        currentIndex: currentIndex,
        changePage: changePage,
      ),
      body: tabs[currentIndex],
    );
  }
}
