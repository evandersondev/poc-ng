import 'package:flutter/material.dart';
import 'package:poc_ng/assets/icons/selection.dart';
import 'package:poc_ng/database/order_product_relationship_database.dart';
import 'package:poc_ng/database/product_database.dart';
import 'package:poc_ng/screens/order/create/widgets/number_picker.dart';
import 'package:poc_ng/shared/models/order_product_relationship_model.dart';
import 'package:poc_ng/shared/models/product_model.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';
import 'package:poc_ng/ui/molecules/custom_snackbar.dart';
import 'package:uuid/uuid.dart';

class ProductScreen extends StatefulWidget {
  final Function setProducts;
  final bool report;
  final ServiceOrderModel order;

  ProductScreen({this.setProducts, this.report, this.order});

  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  List<ProductModel> products = [];

  final searchController = TextEditingController(text: '');
  final descriptionController = TextEditingController(text: '');

  void initState() {
    super.initState();

    initAsync();
  }

  void initAsync() async {
    final response = await ProductDataBase.db.getAllProducts();

    setState(() {
      products = response;
    });
  }

  newProduct(context) async {
    await showDialog(
      context: context,
      builder: (_) {
        return NumberPicker(
          title: descriptionController.text,
          onConfirm: (int count) async {
            final product = ProductModel(
              id: Uuid().v4(),
              description: descriptionController.text,
            );

            await ProductDataBase.db.insertProduct(product);

            widget?.setProducts(
              OrderProductRelationshipModel(
                id: Uuid().v4(),
                productAmount: count,
                serviceOrder: widget.report ? widget.order : null,
                product: product,
                checked: widget.report ? true : false,
              ),
            );

            ScaffoldMessenger.of(context).showSnackBar(
              snackbar(
                  color: Colors.green,
                  message: 'Novo produto criado com sucesso.'),
            );

            Navigator.of(context).pop();
          },
        );
      },
    );
  }

  int productAmount = 1;

  void incrementProductAmount() {
    if (productAmount == 100) return;
    setState(() {
      productAmount = productAmount + 1;
    });
  }

  void decrementProductAmount() {
    if (productAmount == 1) return;
    setState(() {
      productAmount = productAmount - 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final space = SizedBox(height: 20);

    return Scaffold(
      appBar: AppBar(
        title: Text('Produtos'),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      backgroundColor: theme.appBarTheme.shadowColor,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              TextField(
                controller: searchController,
                decoration: InputDecoration(
                  hintText: 'Search',
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                ),
              ),
              space,
              TextField(
                controller: descriptionController,
                decoration: InputDecoration(
                  hintText: 'Novo produto (descrição)',
                  suffixIcon: InkWell(
                    onTap: () async {
                      if (descriptionController.text != '') {
                        newProduct(context);
                      }
                    },
                    child: Icon(
                      Icons.add_box,
                      color: theme.primaryColor,
                    ),
                  ),
                ),
              ),
              SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 30),
                child: Column(
                  children: [
                    ...List.generate(products.length, (index) {
                      return Card(
                        margin: EdgeInsets.only(bottom: 15),
                        elevation: 2,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 2.0),
                          child: ListTile(
                              leading: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Icon(
                                  Icomoon.product,
                                  color: theme.primaryTextTheme.bodyText1.color,
                                ),
                              ),
                              title: Text(
                                products[index].description,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              onTap: () async {
                                await showDialog(
                                  context: context,
                                  builder: (_) {
                                    return NumberPicker(
                                      title: products[index].description,
                                      onConfirm: (int count) async {
                                        final relationship =
                                            OrderProductRelationshipModel(
                                          id: Uuid().v4(),
                                          productAmount: count,
                                          serviceOrder: widget.report
                                              ? widget.order
                                              : null,
                                          product: products[index],
                                          checked: widget.report ? true : false,
                                        );

                                        if (widget.report) {
                                          await OrderProductRelationshipDatabse
                                              .db
                                              .insertRelationship(relationship);
                                        }

                                        widget?.setProducts(relationship);
                                        Navigator.of(context).pop();
                                      },
                                    );
                                  },
                                );
                              }),
                        ),
                      );
                    }).toList()
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
