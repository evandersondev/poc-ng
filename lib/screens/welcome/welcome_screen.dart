import 'package:flutter/material.dart';
import 'package:poc_ng/assets/images.dart';
import 'package:poc_ng/screens/login/login_screen.dart';
import 'package:poc_ng/ui/molecules/button.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  double ratting = 0.0;

  void changeRatting(double value) {
    setState(() {
      ratting = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (ratting == 0.0) {
      return WelcomeTemplate(
        ratting: ratting,
        message: 'Tenha suas ordens de serviço em sua mão',
        nextPage: changeRatting,
        image: welcomeOne,
      );
    } else if (ratting == 1.0) {
      return WelcomeTemplate(
        nextPage: changeRatting,
        ratting: ratting,
        message: 'Ordens de serviço organizadas e sem atrasos',
        image: welcomeTwo,
      );
    } else {
      return LoginScreen();
    }
  }
}

class WelcomeTemplate extends StatelessWidget {
  final String image;
  final String message;
  final double ratting;
  final Function nextPage;

  WelcomeTemplate({
    @required this.image,
    @required this.message,
    @required this.nextPage,
    this.ratting = 0,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height / 1.6,
              child: Image.asset(image, fit: BoxFit.cover),
            ),
            Padding(
              padding: const EdgeInsets.all(30),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    message,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Container(
                    width: 90,
                    child: Slider(
                      value: ratting,
                      onChanged: (value) {
                        nextPage(value);
                      },
                      min: 0.0,
                      max: 1.0,
                      divisions: 1,
                      activeColor: theme.primaryColor,
                      inactiveColor: theme.primaryColor,
                    ),
                  ),
                  Button(
                    title: 'Seguinte',
                    width: MediaQuery.of(context).size.width / 1.5,
                    onPressed: () => nextPage(ratting == 1.0 ? 2.0 : 1.0),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
