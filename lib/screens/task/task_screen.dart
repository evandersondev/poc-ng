import 'package:flutter/material.dart';
import 'package:poc_ng/assets/icons/selection.dart';
import 'package:poc_ng/database/order_task_relationship_database.dart';
import 'package:poc_ng/database/task_database.dart';
import 'package:poc_ng/shared/models/order_task_relationship_model.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';
import 'package:poc_ng/shared/models/task_model.dart';
import 'package:poc_ng/ui/molecules/custom_snackbar.dart';
import 'package:uuid/uuid.dart';

class TaskScreen extends StatefulWidget {
  final Function setTasks;
  final bool report;
  final ServiceOrderModel order;

  TaskScreen({this.setTasks, this.report, this.order});

  @override
  _TaskScreenState createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  List<TaskModel> tasks = [];

  final searchController = TextEditingController(text: '');
  final descriptionController = TextEditingController(text: '');

  void initState() {
    super.initState();

    initAsync();
  }

  void initAsync() async {
    final response = await TaskDataBase.db.getAllTasks();

    setState(() {
      tasks = response;
    });
  }

  void newTask() async {
    final task = TaskModel(
      id: Uuid().v4(),
      description: descriptionController.text,
    );

    final relationship = OrderTaskRelationshipModel(
      id: Uuid().v4(),
      serviceOrder: widget.report ? widget.order : null,
      task: task,
      checked: widget.report ? true : false,
    );

    widget?.setTasks(relationship);

    await TaskDataBase.db.insertTask(task);

    if (widget.report) {
      await OrderTaskRelationshipDatabse.db.insertRelationship(relationship);
    }

    ScaffoldMessenger.of(context).showSnackBar(
      snackbar(color: Colors.green, message: 'Novo tarefa criada com sucesso.'),
    );

    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final space = SizedBox(height: 20);

    return Scaffold(
      appBar: AppBar(
        title: Text('Tarefas'),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      backgroundColor: theme.appBarTheme.shadowColor,
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              TextField(
                controller: searchController,
                decoration: InputDecoration(
                  hintText: 'Search',
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                ),
              ),
              space,
              TextField(
                controller: descriptionController,
                decoration: InputDecoration(
                  hintText: 'Nova tarefa',
                  suffixIcon: InkWell(
                    onTap: () {
                      if (descriptionController.text != '') {
                        newTask();
                      }
                    },
                    child: Icon(
                      Icons.add_box,
                      color: theme.primaryColor,
                    ),
                  ),
                ),
              ),
              SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 30),
                child: Column(
                  children: [
                    ...List.generate(tasks.length, (index) {
                      return Card(
                        margin: EdgeInsets.only(bottom: 15),
                        elevation: 2,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 2.0),
                          child: ListTile(
                              leading: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Icon(
                                  Icomoon.document,
                                  color: theme.primaryTextTheme.bodyText1.color,
                                ),
                              ),
                              title: Text(
                                tasks[index].description,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              onTap: () async {
                                final relationship = OrderTaskRelationshipModel(
                                  id: Uuid().v4(),
                                  task: tasks[index],
                                  serviceOrder:
                                      widget.report ? widget.order : null,
                                  checked: widget.report ? true : false,
                                );

                                if (widget.report) {
                                  await OrderTaskRelationshipDatabse.db
                                      .insertRelationship(relationship);
                                }

                                widget?.setTasks(relationship);
                                Navigator.of(context).pop();
                              }),
                        ),
                      );
                    }).toList()
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
