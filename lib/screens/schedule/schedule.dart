import 'package:flutter/material.dart';
import 'package:poc_ng/assets/icons/selection.dart';
import 'package:poc_ng/database/service_order_database.dart';
import 'package:poc_ng/screens/order/create/order_create_screen.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';
import 'package:poc_ng/ui/templates/card_order.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/intl.dart';

class ScheduleScreen extends StatefulWidget {
  @override
  _ScheduleScreenState createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen> {
  CalendarFormat calendarFormat = CalendarFormat.week;
  DateTime _selectedDay;
  DateTime _focusedDay = DateTime.now();
  List<ServiceOrderModel> serviceOrders;
  List<ServiceOrderModel> ordersToShowing = [];
  bool loading = false;

  void getListServiceOrder() async {
    setState(() {
      loading = true;
    });

    final response = await ServiceOrderDatabase.db.getAllServiceOrder();
    final ordersToJsonList = response.map((e) => e.toJson()).toList();

    if (response != null) {
      final ordersFilteredOpen = ordersToJsonList
          .where((element) => element['status'] == 'open')
          .toList();
      final ordersToModelType =
          ordersFilteredOpen.map((e) => ServiceOrderModel.fromJson(e)).toList();

      setState(() {
        serviceOrders = ordersToModelType;
        loading = false;
      });
      loadingOrdersToShow(_focusedDay);
    }

    setState(() {
      loading = false;
    });
  }

  void initState() {
    super.initState();

    getListServiceOrder();
  }

  void loadingOrdersToShow(DateTime day) {
    setState(() {
      loading = true;
      ordersToShowing = [];
    });

    for (var i = 0; i < serviceOrders.length; i++) {
      final dateOrder = DateFormat('dd/MM/y', 'pt-BR')
          .format(DateTime.parse(serviceOrders[i].expectedStart));
      final date = DateFormat('dd/MM/y', 'pt-BR').format(day);

      if (dateOrder == date) {
        setState(() {
          loading = false;
          ordersToShowing = [...ordersToShowing, serviceOrders[i]];
        });
      }
    }

    setState(() {
      loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final isDarkTheme = theme.brightness == Brightness.dark;
    final color = isDarkTheme ? Colors.red : theme.primaryColor;

    final space = SizedBox(height: 15);

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Container(
                margin: EdgeInsets.only(right: 15),
                child: Icon(Icomoon.calendar)),
            Text('Agenda',
                style: TextStyle(color: theme.textTheme.bodyText1.color)),
          ],
        ),
        automaticallyImplyLeading: false,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icomoon.add, color: Colors.white),
        backgroundColor: color,
        onPressed: () =>
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) {
          return OrderCreateScreen(date: _selectedDay ?? DateTime.now());
        })),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.only(right: 15, left: 15, bottom: 15),
          child: Column(
            children: [
              TableCalendar(
                locale: 'pt_BR',
                availableCalendarFormats: {
                  CalendarFormat.week: 'Semana',
                  CalendarFormat.month: 'Mês'
                },
                calendarFormat: calendarFormat,
                onFormatChanged: (CalendarFormat format) {
                  setState(() {
                    calendarFormat = format;
                  });
                },
                selectedDayPredicate: (day) {
                  return isSameDay(_selectedDay, day);
                },
                onDaySelected: (selectedDay, focusedDay) {
                  loadingOrdersToShow(selectedDay);

                  setState(() {
                    _selectedDay = selectedDay;
                    _focusedDay = focusedDay;
                  });
                },
                firstDay: DateTime.utc(2010, 10, 16),
                lastDay: DateTime.utc(2030, 3, 14),
                focusedDay: _selectedDay ?? _focusedDay,
              ),
              space,
              !loading
                  ? ordersToShowing.length > 0
                      ? Column(
                          children: [
                            ...ordersToShowing.map((item) {
                              return CardOrder(
                                serviceOrder: item,
                                pageIndex: 2,
                              );
                            }),
                          ],
                        )
                      : Container(
                          margin: EdgeInsets.only(top: 15),
                          child: Text(
                            'Nenhuma ordem prevista.',
                            style: TextStyle(fontSize: 18),
                          ),
                        )
                  : Center(child: CircularProgressIndicator()),
            ],
          ),
        ),
      ),
    );
  }
}
