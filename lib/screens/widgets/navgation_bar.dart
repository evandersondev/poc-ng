import 'package:flutter/material.dart';
import 'package:poc_ng/assets/icons/selection.dart';

class NavigationBar extends StatelessWidget {
  final int currentIndex;
  final Function changePage;

  NavigationBar({
    @required this.currentIndex,
    @required this.changePage,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final isDarkTheme = theme.brightness == Brightness.dark;
    final selected = isDarkTheme ? Colors.red : theme.primaryColorDark;
    final unselected = theme.iconTheme.color.withOpacity(0.4);

    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.home_rounded), label: 'Início'),
        BottomNavigationBarItem(icon: Icon(Icomoon.clipboard), label: 'Ordens'),
        BottomNavigationBarItem(icon: Icon(Icomoon.calendar), label: 'Agenda'),
      ],
      elevation: 15,
      // type: BottomNavigationBarType.fixed,
      backgroundColor: theme.appBarTheme.color,
      currentIndex: currentIndex,
      selectedItemColor: selected,
      unselectedItemColor: unselected,
      selectedLabelStyle: TextStyle(fontSize: 11, height: 1.6),
      unselectedLabelStyle: TextStyle(fontSize: 11, height: 1.6),
      onTap: (index) {
        changePage(index);
      },
    );
  }
}
