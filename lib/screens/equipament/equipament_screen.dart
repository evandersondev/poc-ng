import 'package:flutter/material.dart';
import 'package:poc_ng/assets/icons/selection.dart';
import 'package:poc_ng/shared/models/equipament_model.dart';
import 'package:poc_ng/ui/molecules/custom_snackbar.dart';
import 'package:poc_ng/database/equipament_database.dart';
import 'package:uuid/uuid.dart';

class EquipamentScreen extends StatefulWidget {
  final Function setEquipament;
  EquipamentScreen({this.setEquipament});

  @override
  _EquipamentScreenState createState() => _EquipamentScreenState();
}

class _EquipamentScreenState extends State<EquipamentScreen> {
  List<EquipamentModel> equipaments = [];

  final searchController = TextEditingController(text: '');
  final descriptionController = TextEditingController(text: '');

  void getEquipaments() async {
    final response = await EquipamentDatabase.db.listAllEquipaments();
    if (response != null) {
      final data = response.map((e) => EquipamentModel.fromJson(e)).toList();
      setState(() {
        equipaments = data;
      });
    }
  }

  void createEquipament(context) async {
    final equipament = EquipamentModel(
      id: Uuid().v4(),
      code: 1.toString(),
      description: descriptionController.text,
    );

    final response = await EquipamentDatabase.db.insertEquipament(equipament);
    if (response != null) {
      ScaffoldMessenger.of(context).showSnackBar(
        snackbar(
            color: Colors.green,
            message: 'Equipamento #${response.code} criado com sucesso.'),
      );

      widget?.setEquipament(equipament);
      Navigator.of(context).pop();
    }
  }

  void initState() {
    super.initState();

    getEquipaments();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final space = SizedBox(height: 20);

    return Scaffold(
      appBar: AppBar(
        title: Text('Equipamentos'),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      backgroundColor: theme.appBarTheme.shadowColor,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              TextField(
                controller: searchController,
                decoration: InputDecoration(
                  hintText: 'Search',
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                ),
              ),
              space,
              TextField(
                controller: descriptionController,
                decoration: InputDecoration(
                  hintText: 'Novo equipamento (descrição)',
                  suffixIcon: InkWell(
                    onTap: () => descriptionController.text != ''
                        ? createEquipament(context)
                        : null,
                    child: Icon(
                      Icons.add_box,
                      color: theme.primaryColor,
                    ),
                  ),
                ),
              ),
              SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 30),
                child: Column(
                  children: [
                    ...List.generate(equipaments.length, (index) {
                      return Card(
                        margin: EdgeInsets.only(bottom: 15),
                        elevation: 2,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 2.0),
                          child: ListTile(
                              leading: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Icon(
                                  Icomoon.equipment,
                                  color: theme.primaryTextTheme.bodyText1.color,
                                ),
                              ),
                              title: Text(
                                equipaments[index].description,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(height: 8),
                                  Text(equipaments[index].code),
                                ],
                              ),
                              onTap: () {
                                widget.setEquipament(equipaments[index]);
                                Navigator.of(context).pop();
                              }),
                        ),
                      );
                    }).toList()
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
