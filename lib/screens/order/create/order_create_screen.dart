import 'dart:io';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:poc_ng/database/order_product_relationship_database.dart';
import 'package:poc_ng/database/order_task_relationship_database.dart';
import 'package:poc_ng/screens/equipament/equipament_screen.dart';
import 'package:poc_ng/screens/index.dart';
import 'package:poc_ng/screens/order/manage/order_manage_screen.dart';
import 'package:poc_ng/shared/models/equipament_model.dart';
import 'package:poc_ng/shared/models/order_product_relationship_model.dart';
import 'package:poc_ng/shared/models/order_task_relationship_model.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';
import 'package:poc_ng/shared/themes/app_colors.dart';
import 'package:poc_ng/ui/molecules/button.dart';
import 'package:poc_ng/ui/molecules/custom_snackbar.dart';
import 'package:poc_ng/ui/organisms/date_picker.dart';
import 'package:poc_ng/ui/organisms/time_picker.dart';
import 'package:poc_ng/ui/templates/modal_picture.dart';
import 'package:poc_ng/database/service_order_database.dart';
import 'package:expandable/expandable.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

import 'widgets/resources_order.dart';

class OrderCreateArguments {
  final DateTime date;
  final int pageIndex;

  OrderCreateArguments({this.date, this.pageIndex});
}

class OrderCreateScreen extends StatefulWidget {
  final DateTime date;
  final int pageIndex;

  OrderCreateScreen({this.date, this.pageIndex});

  @override
  _OrderCreateScreenState createState() => _OrderCreateScreenState();
}

class _OrderCreateScreenState extends State<OrderCreateScreen> {
  EquipamentModel equipament;
  List<OrderProductRelationshipModel> products = [];
  List<OrderTaskRelationshipModel> tasks = [];
  List<File> images = [];

  DateTime date;
  TimeOfDay time;

  bool validate = false;
  bool loading = false;

  final scrollDirection = Axis.vertical;
  AutoScrollController controller;

  final descriptionController = TextEditingController();
  final equipamentController = TextEditingController();
  final localizationController = TextEditingController();

  void selecteDate(DateTime value) {
    setState(() {
      date = value;
    });
  }

  void selecteTime(TimeOfDay value) {
    setState(() {
      time = value;
    });
  }

  void setImage(File image) {
    setState(() {
      images = [...images, image];
    });
  }

  Future<void> saveImages(ServiceOrderModel order) async {
    final prefs = await SharedPreferences.getInstance();
    final localstorage = prefs.getString('order_images_relationship');
    final relationship = images
        .map((item) => {'service_order_id': order.id, 'image': item.path})
        .toList();

    if (localstorage == null) {
      prefs.setString('order_images_relationship', json.encode(relationship));
    } else {
      prefs.setString('order_images_relationship',
          json.encode([...json.decode(localstorage), ...relationship]));
    }
  }

  void setEquipament(EquipamentModel value) {
    setState(() {
      equipament = value;
    });

    equipamentController.text = value.description;
    localizationController.text = value.localization;
  }

  void setProducts(OrderProductRelationshipModel relationship) {
    if (products.length > 0) {
      setState(() {
        products = [...products, relationship];
      });
    } else {
      setState(() {
        products = [relationship];
      });
    }
  }

  void setTasks(OrderTaskRelationshipModel relationship) {
    if (tasks.length > 0) {
      setState(() {
        tasks = [...tasks, relationship];
      });
    } else {
      setState(() {
        tasks = [relationship];
      });
    }
  }

  void openServiceOrder() async {
    setState(() {
      loading = true;
    });

    final order = ServiceOrderModel(
      id: Uuid().v4(),
      code: 1.toString(),
      description: descriptionController.text,
      equipament: equipament,
      localization: localizationController.text,
      expectedStart: DateTime.parse(
              '${DateFormat('yyyy-MM-dd', 'pt-BR').format(DateTime.parse(date.toString()))} ${time.format(context)}:00')
          .toIso8601String(),
    );
    final response = await ServiceOrderDatabase.db.createOrder(order);

    if (response != null) {
      if (images.length > 0) {
        await saveImages(order);
      }

      if (products.length > 0) {
        for (var i = 0; i < products.length; i++) {
          final relationship = OrderProductRelationshipModel(
            id: Uuid().v4(),
            productAmount: products[i].productAmount,
            product: products[i].product,
            serviceOrder: order,
          );

          await OrderProductRelationshipDatabse.db
              .insertRelationship(relationship);
        }
      }

      if (tasks.length > 0) {
        for (var i = 0; i < tasks.length; i++) {
          final relationship = OrderTaskRelationshipModel(
            id: Uuid().v4(),
            serviceOrder: order,
            task: tasks[i].task,
          );

          await OrderTaskRelationshipDatabse.db
              .insertRelationship(relationship);
        }
      }

      setState(() {
        loading = false;
      });

      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (_) => OrderManageScreen(order: response, pageIndex: 0),
        ),
      );

      ScaffoldMessenger.of(context).showSnackBar(
        snackbar(
            color: Colors.green,
            message: 'Ordem #${response.code} criada com sucesso.'),
      );
    } else {
      print('Error');
    }
  }

  @override
  void initState() {
    super.initState();

    if (widget.date != null) {
      setState(() {
        date = widget.date;
      });
    }

    controller = AutoScrollController(
        viewportBoundaryGetter: () =>
            Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
        axis: scrollDirection);
  }

  Future scrollToIndex() async {
    await controller.scrollToIndex(0, preferPosition: AutoScrollPosition.begin);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final space = SizedBox(
      height: 20,
    );

    if (date != null && time != null) {
      setState(() {
        validate = true;
      });
      scrollToIndex();
    } else {
      setState(() {
        validate = false;
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Nova Ordem de Serviço',
          style: TextStyle(color: Colors.black),
        ),
        leading: BackButton(
          color: AppColors.heading,
          onPressed: () => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (_) => IndexPages(pageIndex: widget.pageIndex))),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          color: Colors.transparent,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Button(
                title: 'Abrir O.S.',
                width: 100,
                enable: validate,
                onPressed: openServiceOrder,
              )
            ],
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 0),
        child: ListView(
          scrollDirection: scrollDirection,
          controller: controller,
          padding: const EdgeInsets.only(bottom: 30),
          children: [
            !loading
                ? Column(
                    key: ValueKey(0),
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Problema identificado',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                space,
                                TextField(
                                  keyboardType: TextInputType.multiline,
                                  minLines: 5,
                                  maxLines: 5,
                                  autofocus: false,
                                  controller: descriptionController,
                                  decoration: InputDecoration(
                                    hintText: 'Descrição',
                                    fillColor: Colors.orange,
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(2.0),
                                    ),
                                  ),
                                ),
                                space,
                                TextField(
                                  readOnly: true,
                                  controller: equipamentController,
                                  onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => EquipamentScreen(
                                        setEquipament: setEquipament,
                                      ),
                                    ),
                                  ),
                                  decoration: InputDecoration(
                                    hintText: 'Equipamento (opcional)',
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(2.0),
                                    ),
                                  ),
                                ),
                                space,
                                TextField(
                                  controller: localizationController,
                                  decoration: InputDecoration(
                                    hintText: 'Localização (opcional)',
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(2.0),
                                    ),
                                  ),
                                ),
                                space,
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Execução prevista para: ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      space,
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          DatePicker(
                                            title: '   /   /   ',
                                            value: date,
                                            onChanged: selecteDate,
                                          ),
                                          TimePicker(
                                            title: '00:00',
                                            value: time,
                                            onChanged: selecteTime,
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                space,
                                ModalPicturesGrid(
                                  images: images,
                                  setImage: setImage,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      space,
                    ],
                  )
                : Center(child: CircularProgressIndicator()),
            AutoScrollTag(
              key: ValueKey(1),
              controller: controller,
              index: 1,
              child: ExpandableNotifier(
                child: ScrollOnExpand(
                    scrollOnExpand: false,
                    child: ExpandablePanel(
                      header: Text(
                        'Recursos',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                        ),
                      ),
                      collapsed: Divider(
                        thickness: 1.5,
                        height: 1,
                        color: theme.textTheme.bodyText1.color,
                      ),
                      expanded: ResourcesOrder(
                        products: products,
                        tasks: tasks,
                        setProducts: setProducts,
                        setTasks: setTasks,
                      ),
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }
}
