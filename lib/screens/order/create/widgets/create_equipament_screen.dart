import 'package:flutter/material.dart';
import 'package:poc_ng/assets/icons/selection.dart';
import 'package:poc_ng/screens/order/create/order_create_screen.dart';
import 'package:poc_ng/shared/models/equipament_model.dart';
import 'package:poc_ng/shared/themes/app_colors.dart';
import 'package:poc_ng/ui/molecules/button.dart';
import 'package:poc_ng/ui/molecules/custom_snackbar.dart';
import 'package:poc_ng/ui/templates/modal_picture.dart';
import 'package:poc_ng/database/equipament_database.dart';
import 'package:uuid/uuid.dart';

class CreateEquipamentScreen extends StatefulWidget {
  CreateEquipamentScreen({@required this.getEquipaments});

  final Function getEquipaments;

  @override
  _CreateEquipamentScreenState createState() => _CreateEquipamentScreenState();
}

class _CreateEquipamentScreenState extends State<CreateEquipamentScreen> {
  final descriptionController = TextEditingController(text: '');
  final localizationController = TextEditingController(text: '');
  bool canSave = false;

  void createEquipament(context) async {
    final equipament = EquipamentModel(
        id: Uuid().v4(),
        code: 1.toString(),
        description: descriptionController.text,
        localization: localizationController.text);

    final response = await EquipamentDatabase.db.insertEquipament(equipament);
    if (response != null) {
      widget.getEquipaments();
      Navigator.of(context).pop();
      ScaffoldMessenger.of(context).showSnackBar(
        snackbar(
            color: Colors.green, message: 'Equipamento criado com sucesso.'),
      );
    }
  }

  Widget equipamentForm() {
    return Center(
      child: Column(
        children: [
          TextField(
            controller: descriptionController,
            decoration: InputDecoration(
              labelText: 'Descrição',
              alignLabelWithHint: true,
              floatingLabelBehavior: FloatingLabelBehavior.always,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(2.0),
              ),
            ),
            onChanged: (value) {
              if (descriptionController.text == '') {
                setState(() {
                  canSave = false;
                });
              } else {
                setState(() {
                  canSave = true;
                });
              }
            },
          ),
          SizedBox(height: 30.0),
          TextField(
            controller: localizationController,
            decoration: InputDecoration(
              labelText: 'Localização (Opcional)',
              // alignLabelWithHint: true,
              suffixIcon: Icon(Icons.location_on),
              floatingLabelBehavior: FloatingLabelBehavior.always,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(2.0),
              ),
            ),
          ),
          SizedBox(height: 30.0),
          ModalPicturesGrid(images: [])
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Icon(Icomoon.equipment),
            SizedBox(width: 15),
            Text('Novo equipamento'),
          ],
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
      ),
      backgroundColor: theme.appBarTheme.backgroundColor,
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(
                  'Cancelar',
                  style: TextStyle(color: theme.textTheme.bodyText1.color),
                )),
            SizedBox(width: 20),
            Button(
              title: 'Criar equipamento',
              enable: canSave,
              width: 180,
              onPressed: () => createEquipament(context),
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 30.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              equipamentForm(),
            ],
          ),
        ),
      ),
    );
  }
}
