import 'package:flutter/material.dart';
import 'package:poc_ng/screens/task/task_screen.dart';
import 'package:poc_ng/shared/models/order_product_relationship_model.dart';
import 'package:poc_ng/shared/models/order_task_relationship_model.dart';
import 'package:poc_ng/ui/molecules/button.dart';

import '../../../product/product_screen.dart';

class ResourcesOrder extends StatelessWidget {
  final List<OrderProductRelationshipModel> products;
  final List<OrderTaskRelationshipModel> tasks;
  final Function setProducts;
  final Function setTasks;

  ResourcesOrder({
    @required this.products,
    @required this.tasks,
    this.setProducts,
    this.setTasks,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return ConstrainedBox(
      constraints: BoxConstraints(
        minHeight: MediaQuery.of(context).size.height / 1.6,
        maxHeight: MediaQuery.of(context).size.height / 1.6,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Divider(
            thickness: 1.5,
            height: 1,
            color: theme.textTheme.bodyText1.color,
          ),
          SizedBox(height: 30.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Produtos',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              Container(
                height: 42,
                child: Button(
                    title: 'Produto +',
                    width: 100,
                    backgroundColor: Colors.transparent,
                    textStyle: TextStyle(color: Colors.black),
                    onPressed: () => Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (_) => ProductScreen(
                              setProducts: setProducts,
                              report: false,
                            ),
                          ),
                        )),
              ),
            ],
          ),
          SizedBox(height: 30.0),
          products != null
              ? Column(
                  children: [
                    ...products.map((relationship) {
                      return Container(
                        padding: const EdgeInsets.only(right: 14.0, left: 10.0),
                        margin: EdgeInsets.only(bottom: 12.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              relationship.product.description,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text(
                              '${relationship.productAmount} UN',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      );
                    }).toList()
                  ],
                )
              : SizedBox(),
          SizedBox(height: 30.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Tarefas',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  )),
              Container(
                height: 42,
                child: Button(
                    title: 'Tarefas +',
                    width: 100,
                    backgroundColor: Colors.transparent,
                    textStyle: TextStyle(color: Colors.black),
                    onPressed: () => Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => TaskScreen(
                              setTasks: setTasks,
                              report: false,
                            ),
                          ),
                        )),
              ),
            ],
          ),
          SizedBox(height: 30.0),
          tasks != null
              ? Column(
                  children: [
                    ...tasks.map((relationship) {
                      return Container(
                        padding: const EdgeInsets.only(right: 14.0, left: 10.0),
                        margin: EdgeInsets.only(bottom: 12.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              relationship.task.description,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      );
                    }).toList(),
                  ],
                )
              : SizedBox(),
        ],
      ),
    );
  }
}
