import 'package:flutter/material.dart';
import 'package:poc_ng/ui/molecules/button.dart';

class NumberPicker extends StatefulWidget {
  final String title;
  final Function onConfirm;

  NumberPicker({@required this.title, @required this.onConfirm});
  @override
  _NumberPickerState createState() => _NumberPickerState();
}

class _NumberPickerState extends State<NumberPicker> {
  int count = 1;

  void incrementCout() {
    if (count == 100) return;
    setState(() {
      count = count + 1;
    });
  }

  void decrementCount() {
    if (count == 1) return;
    setState(() {
      count = count - 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(2),
      ),
      elevation: 14,
      content: contentBox(context),
      title: Text(
        '${widget.title}',
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
        ),
      ),
      actions: [
        InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Text('Cancelar'),
        ),
        SizedBox(width: 8),
        Container(
          height: 38,
          width: 100,
          child: Button(
            title: 'Confirmar',
            onPressed: () {
              widget.onConfirm(count);
              Navigator.of(context).pop();
            },
          ),
        ),
      ],
    );
  }

  Widget contentBox(BuildContext context) {
    return Container(
      height: 250,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisSize: MainAxisSize.max,
        children: [
          Text(
            '$count',
            style: TextStyle(
              fontSize: 80,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(width: 15),
          Container(
            height: 175,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              // mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  height: 80,
                  width: 80,
                  child: InkWell(
                    onTap: incrementCout,
                    child: Icon(Icons.add, size: 80),
                  ),
                ),
                Container(
                  height: 80,
                  width: 80,
                  child: InkWell(
                    onTap: decrementCount,
                    child: Icon(Icons.remove, size: 80),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
