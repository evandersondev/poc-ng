import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:poc_ng/assets/icons/selection.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';
import 'package:poc_ng/ui/templates/card_order.dart';
import 'package:poc_ng/database/service_order_database.dart';

import '../create/order_create_screen.dart';

class OrderListScreen extends StatefulWidget {
  @override
  _OrderListScreenState createState() => _OrderListScreenState();
}

class _OrderListScreenState extends State<OrderListScreen> {
  List<ServiceOrderModel> serviceOrders;
  List<ServiceOrderModel> ordersToShow;
  bool loading = false;
  final searchController = TextEditingController();

  void getListServiceOrder() async {
    final response = await ServiceOrderDatabase.db.getAllServiceOrder();
    if (response != null) {
      setState(() {
        serviceOrders = response;
        ordersToShow = response;
      });
    }

    setState(() {
      loading = false;
    });
  }

  void initState() {
    super.initState();

    setState(() {
      loading = true;
    });

    getListServiceOrder();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final isDarkTheme = theme.brightness == Brightness.dark;
    final color = isDarkTheme ? Colors.red : theme.primaryColor;

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Container(
                margin: EdgeInsets.only(right: 15),
                child: Icon(Icomoon.clipboard)),
            Text('Ordens de Serviço',
                style: TextStyle(color: theme.textTheme.bodyText1.color)),
          ],
        ),
        automaticallyImplyLeading: false,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icomoon.add,
          color: Colors.white,
        ),
        backgroundColor: color,
        onPressed: () {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (context) => OrderCreateScreen(pageIndex: 1),
            ),
          );
        },
      ),
      body: !loading
          ? SingleChildScrollView(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 30),
                child: Column(
                  children: [
                    Container(
                      height: 56,
                      child: TextField(
                        controller: searchController,
                        onChanged: (value) {
                          final filter = serviceOrders.where((item) {
                            return item.description
                                    .toLowerCase()
                                    .contains(value.toLowerCase()) ||
                                item.code.contains(value);
                          }).toList();

                          if (filter.length > 0) {
                            setState(() {
                              ordersToShow = filter;
                            });
                          } else {
                            setState(() {
                              ordersToShow = [];
                            });
                          }
                        },
                        decoration: InputDecoration(
                          enabled: true,
                          hintText: 'Search',
                          prefixIcon: Icon(Icons.search),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(1.0),
                          ),
                          suffixIcon: IconButton(
                              onPressed: () {
                                searchController.clear();
                                setState(() {
                                  ordersToShow = serviceOrders;
                                });
                              },
                              icon: Icon(Icomoon.close)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    ordersToShow.length > 0
                        ? Column(
                            children: [
                              ...List.generate(ordersToShow.length, (index) {
                                return ordersToShow.length > 0
                                    ? CardOrder(
                                        serviceOrder: ordersToShow[index],
                                        pageIndex: 1,
                                      )
                                    : Center(
                                        child: Text('Nenhuma order'),
                                      );
                              }).toList()
                            ],
                          )
                        : RichText(
                            text: TextSpan(
                              style: TextStyle(
                                color: theme.textTheme.bodyText1.color,
                                height: 1.2,
                                fontSize: 16,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                    text:
                                        'Não há nenhuma ordem de serviço cadastrada no momento. '),
                                TextSpan(
                                  text: 'Clique aqui ',
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              OrderCreateScreen(),
                                        ),
                                      );
                                    },
                                ),
                                TextSpan(
                                    text:
                                        'para cadastrar uma nova ordem de serviço.'),
                              ],
                            ),
                          )
                  ],
                ),
              ),
            )
          : Center(child: CircularProgressIndicator()),
    );
  }
}
