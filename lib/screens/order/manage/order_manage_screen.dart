import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:poc_ng/database/order_product_relationship_database.dart';
import 'package:poc_ng/database/order_task_relationship_database.dart';
import 'package:poc_ng/screens/index.dart';
import 'package:poc_ng/screens/order/manage/widgets/floating_actions.dart';
import 'package:poc_ng/screens/report/order_report_screen.dart';
import 'package:poc_ng/shared/models/imagesModel.dart';
import 'package:poc_ng/shared/models/order_product_relationship_model.dart';
import 'package:poc_ng/shared/models/order_task_relationship_model.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';
import 'package:poc_ng/shared/themes/app_colors.dart';
import 'package:poc_ng/ui/templates/modal_picture.dart';
import 'package:poc_ng/utils/create_pdf.dart';
import 'package:poc_ng/database/service_order_database.dart';
import 'package:intl/intl.dart';
import 'package:share_extend/share_extend.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'widgets/pdf_viewer.dart';

mixin ResourcesMixin {
  List<OrderProductRelationshipModel> products;
  List<OrderTaskRelationshipModel> tasks;
}

class OrderManageScreen extends StatefulWidget {
  final ServiceOrderModel order;
  final String orderId;
  final int pageIndex;

  OrderManageScreen({this.order, this.orderId, this.pageIndex});

  @override
  _OrderManageScreenState createState() => _OrderManageScreenState();
}

class _OrderManageScreenState extends State<OrderManageScreen>
    with SingleTickerProviderStateMixin {
  ServiceOrderModel serviceOrder;
  List<OrderProductRelationshipModel> products = [];
  List<OrderTaskRelationshipModel> tasks = [];
  List<File> images = [];

  bool loading = true;
  final String notInformated = 'Não informado';

  TabController _tabController;
  final List<Tab> myTabs = <Tab>[
    Tab(text: '0'),
    Tab(text: '1'),
    Tab(text: '2'),
  ];

  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length);
    initAsync();
    loadImages();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void loadImages() async {
    final prefs = await SharedPreferences.getInstance();

    if (prefs.containsKey('order_images_relationship')) {
      final list = prefs.getString('order_images_relationship');
      final List relationship = json.decode(list);
      final relationshipList = relationship.map((item) {
        final model = ImagesModel.fromJson({
          'image': item['image'].toString(),
          'serviceOrderId': item['service_order_id'].toString(),
        });
        return model;
      }).toList();

      final imagesFile = relationshipList
          .where((item) => item.serviceOrderId == widget.order.id)
          .map((item) => item.image)
          .toList();

      setState(() {
        images = imagesFile;
      });
    }
  }

  Widget labelWithValue({String label, String value}) {
    final space = SizedBox(height: 8);
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(
        label,
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      space,
      Text(value ?? 'Não informado'),
    ]);
  }

  Future<List<OrderProductRelationshipModel>> getProductsResources(
      ServiceOrderModel order) async {
    final orderProductRelationship =
        await OrderProductRelationshipDatabse.db.getAllRelationships(order);
    return orderProductRelationship;
  }

  Future<List<OrderTaskRelationshipModel>> getTasksResources(
      ServiceOrderModel order) async {
    final orderTaskRelationship =
        await OrderTaskRelationshipDatabse.db.getAllRelationships(order);
    return orderTaskRelationship;
  }

  void initAsync() async {
    setState(() {
      loading = true;
    });

    if (widget.orderId != null) {
      final response =
          await ServiceOrderDatabase.db.showServiceOrder(widget.orderId);
      setState(() {
        serviceOrder = response;
      });
    }

    if (widget.order != null) {
      final resourcesProducts = await getProductsResources(widget.order);
      final resourcesTasks = await getTasksResources(widget.order);
      setState(() {
        serviceOrder = widget.order;
        products = resourcesProducts;
        tasks = resourcesTasks;
      });
    }
    setState(() {
      loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final space = SizedBox(height: 15);
    final widthSpace = SizedBox(width: 30);

    final productsCheckedList =
        products.where((element) => element.checked == true).toList();

    final tasksCheckedList =
        tasks.where((element) => element.checked == true).toList();

    Future<bool> onWillPop() async {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => IndexPages(pageIndex: widget.pageIndex ?? 1),
        ),
      );

      return true;
    }

    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: BackButton(
            color: AppColors.heading,
            onPressed: () => Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (_) =>
                        IndexPages(pageIndex: widget.pageIndex ?? 1))),
          ),
          title: Text(
            'O.S. ${!loading && serviceOrder != null ? serviceOrder.code : '000000'}',
            style: TextStyle(color: theme.textTheme.bodyText1.color),
          ),
          actions: [
            IconButton(
              icon: Icon(
                Icons.share_rounded,
                color: theme.textTheme.bodyText1.color,
              ),
              onPressed: () async {
                final path =
                    await CreatePdf.pdf.view(serviceOrder, products, tasks);

                ShareExtend.share(
                  path,
                  'file',
                  sharePanelTitle: 'Ordem #${serviceOrder?.code}.pdf',
                );

                // ? PAGE PDF VIEWER - change page after then comment line
                // Navigator.of(context).push(
                //   MaterialPageRoute(
                //     builder: (context) => PdfViewPage(
                //       path: path,
                //       title: 'Ordem #${serviceOrder.code}',
                //       filename: 'Ordem #${serviceOrder.code}.pdf',
                //     ),
                //   ),
                // );
              },
            )
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: FloatingActions(
          order: serviceOrder,
          pageIndex: widget.pageIndex,
          getProductsResources: getProductsResources,
          getTasksResources: getTasksResources,
        ),
        body: SafeArea(
          child: !loading && serviceOrder != null
              ? SingleChildScrollView(
                  padding: const EdgeInsets.only(
                      left: 15, right: 15, top: 15, bottom: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          labelWithValue(
                              label: 'Equipamento',
                              value: serviceOrder?.equipament?.description ??
                                  notInformated),
                          space,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              labelWithValue(
                                  label: 'Problema',
                                  value: serviceOrder?.description ??
                                      notInformated),
                              widthSpace,
                              labelWithValue(
                                  label: 'Localização',
                                  value: serviceOrder.localization ??
                                      serviceOrder.equipament.localization ??
                                      notInformated),
                            ],
                          ),
                          space,
                          Text(
                            'Execução prevista para: ${DateFormat('dd/MM/yyyy', 'pt-BR').format(DateTime.parse(serviceOrder?.expectedStart))} ás ${DateFormat('hh:mm', 'pt-BR').format(DateTime.parse(serviceOrder?.expectedStart))}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          space,
                          Text(
                            'Imagens',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          space,
                          ModalPicturesGrid(
                              images: images ?? [],
                              enable:
                                  serviceOrder.status == 'open' ? true : false),
                          space,
                          serviceOrder.status == 'canceled'
                              ? Container()
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Recursos',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    Divider(
                                      thickness: 2,
                                      color: theme.textTheme.bodyText1.color,
                                    ),
                                    serviceOrder?.report != '' &&
                                            serviceOrder?.report != null
                                        ? Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                                productsCheckedList.length > 0
                                                    ? Column(
                                                        children: [
                                                          Text(
                                                            'Produtos',
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 16,
                                                            ),
                                                          ),
                                                          space,
                                                        ],
                                                      )
                                                    : Container(),
                                                ...productsCheckedList
                                                    .map((relationship) {
                                                  return Container(
                                                    height: 30,
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 20.0,
                                                            left: 10.0),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Container(
                                                          height: 25,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width -
                                                              120,
                                                          child: Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .max,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Text(
                                                                relationship
                                                                    .product
                                                                    .description,
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                              Text(
                                                                '${relationship.productAmount} UN',
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Container(
                                                          width: 20,
                                                          child: Checkbox(
                                                            checkColor:
                                                                Colors.white,
                                                            value: relationship
                                                                .checked,
                                                            onChanged: null,
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  );
                                                }).toList(),
                                                space,
                                                tasksCheckedList.length > 0
                                                    ? Text('Tarefas',
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 16,
                                                        ))
                                                    : Container(),
                                                space,
                                                ...tasksCheckedList
                                                    .map((relationship) {
                                                  return Container(
                                                    height: 20,
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 14.0,
                                                            left: 10.0),
                                                    margin: EdgeInsets.only(
                                                        bottom: 12.0),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text(
                                                          relationship
                                                              .task.description,
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                        Checkbox(
                                                          checkColor:
                                                              Colors.white,
                                                          value: relationship
                                                              .checked,
                                                          onChanged: null,
                                                        ),
                                                      ],
                                                    ),
                                                  );
                                                }).toList(),
                                                space,
                                                Container(
                                                  padding: EdgeInsets.only(
                                                      right: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      Text(
                                                        'Custo total: ${serviceOrder.report}',
                                                        style: TextStyle(
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                )
                                              ])
                                        : serviceOrder.status == 'closed'
                                            ? Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'Não informado',
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                  space,
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 10),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          'Custo total: R\$ 0.00',
                                                          style: TextStyle(
                                                            fontSize: 18,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              )
                                            : Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                      'Nenhum recursos reportado até o momento',
                                                      style: TextStyle(
                                                          height: 1.5)),
                                                  InkWell(
                                                    child: Text(
                                                      'Clique para reportar.',
                                                      textAlign:
                                                          TextAlign.start,
                                                      style: TextStyle(
                                                        color: Colors.blue[700],
                                                        decoration:
                                                            TextDecoration
                                                                .underline,
                                                      ),
                                                    ),
                                                    onTap: () =>
                                                        Navigator.of(context)
                                                            .pushReplacement(
                                                      MaterialPageRoute(
                                                        builder: (_) =>
                                                            OrderReportScreen(
                                                          order: serviceOrder,
                                                          pageIndex:
                                                              widget.pageIndex,
                                                          getProductsResources:
                                                              getProductsResources,
                                                          getTasksResources:
                                                              getTasksResources,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                  ],
                                )
                        ],
                      ),
                    ],
                  ),
                )
              : Center(child: CircularProgressIndicator()),
        ),
      ),
    );
  }
}
