import 'package:floating_action_bubble/floating_action_bubble.dart';
import 'package:flutter/material.dart';
import 'package:poc_ng/screens/index.dart';
import 'package:poc_ng/screens/report/order_report_screen.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';
import 'package:poc_ng/ui/colors.dart';
import 'package:poc_ng/ui/molecules/custom_snackbar.dart';
import 'package:poc_ng/ui/organisms/alert.dart';
import 'package:poc_ng/database/service_order_database.dart';

Map statusMap = {
  'open': 'aberta',
  'closed': 'finalizada',
  'canceled': 'cancelada',
};

class FloatingActions extends StatefulWidget {
  final ServiceOrderModel order;
  final Function getProductsResources;
  final Function getTasksResources;
  final int pageIndex;

  FloatingActions({
    this.order,
    @required this.pageIndex,
    @required this.getProductsResources,
    @required this.getTasksResources,
  });
  @override
  _FloatingActionsState createState() => _FloatingActionsState();
}

class _FloatingActionsState extends State<FloatingActions>
    with SingleTickerProviderStateMixin {
  Animation<double> _animation;

  AnimationController _animationController;
  final descriptionController = TextEditingController(text: '');

  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 0),
    );

    final curvedAnimation =
        CurvedAnimation(curve: Curves.easeInOut, parent: _animationController);

    _animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);
  }

  void finalizeServiceOrder() async {
    final order = widget.order.toJson();

    await ServiceOrderDatabase.db.updateOrder(
      ServiceOrderModel.fromJson(
        {
          ...order,
          'status': 'closed',
          'details': descriptionController.text,
          'doneDate': '${DateTime.now()}',
        },
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(
      snackbar(
          color: Colors.green,
          message: 'O.S. #${widget.order.code} Finalizada.'),
    );
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => IndexPages(pageIndex: 1),
      ),
    );
  }

  void cancelServiceOrder() async {
    final order = widget.order.toJson();
    await ServiceOrderDatabase.db.updateOrder(
      ServiceOrderModel.fromJson(
        {
          ...order,
          'status': 'canceled',
          'reason': descriptionController.text,
        },
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(
      snackbar(
          color: Colors.green,
          message: 'O.S. #${widget.order.code} Cancelada.'),
    );
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => IndexPages(pageIndex: 1),
      ),
    );
  }

  Future alertToFinalizeAndToCancelOrder(
      {String title,
      String placeholder,
      String confirm,
      Function onConfirm}) async {
    return await Alert.showAlert(
        context: context,
        title: title,
        content: TextField(
          keyboardType: TextInputType.multiline,
          minLines: 5,
          maxLines: 5,
          autofocus: false,
          controller: descriptionController,
          decoration: InputDecoration(
            hintText: placeholder,
            fillColor: Colors.orange,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(2.0),
            ),
          ),
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Text('Cancelar'),
          ),
          SizedBox(width: 8),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            height: 38,
            decoration: BoxDecoration(
              color: NGColors.darkBlue,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(2),
            ),
            child: TextButton(
              onPressed: onConfirm,
              child: Text(
                confirm,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ]);
  }

  Future routeAlreadyExecuted({String message}) async {
    return await Alert.showAlert(
        context: context,
        title: 'Error',
        content: Text(message),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Text('OK'),
          ),
        ]);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final isDarkTheme = theme.brightness == Brightness.dark;
    final color = isDarkTheme ? Colors.red : theme.primaryColor;

    return FloatingActionBubble(
      items: [
        Bubble(
          title: 'Reportar',
          iconColor: Colors.white,
          bubbleColor: color,
          icon: Icons.play_arrow,
          titleStyle: const TextStyle(fontSize: 14, color: Colors.white),
          onPress: widget?.order?.status == 'open' &&
                  (widget.order.report == null || widget.order.report == '')
              ? () {
                  setState(() {
                    _animationController.reverse();
                  });

                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => OrderReportScreen(
                        order: widget?.order,
                        pageIndex: widget.pageIndex,
                        getProductsResources: widget.getProductsResources,
                        getTasksResources: widget.getTasksResources,
                      ),
                    ),
                  );
                }
              : widget?.order?.status == 'open' &&
                      (widget.order.report != null || widget.order.report != '')
                  ? () async => await routeAlreadyExecuted(
                      message:
                          'A ordem de serviço #${widget.order.code} já foi reportada.')
                  : () async => await routeAlreadyExecuted(
                      message:
                          'A ordem de serviço #${widget.order.code} já foi ${statusMap[widget.order.status]}.'),
        ),
        Bubble(
          title: 'Finalizar',
          iconColor: Colors.white,
          bubbleColor: color,
          icon: Icons.done_all,
          titleStyle: const TextStyle(fontSize: 14, color: Colors.white),
          onPress: widget?.order?.status == 'open'
              ? () async {
                  setState(() {
                    _animationController.reverse();
                  });

                  await alertToFinalizeAndToCancelOrder(
                      title: 'Finalizando O.S ${widget.order.code}',
                      placeholder:
                          'Descreva o serviço realizado ou outras anotações (opcional)',
                      confirm: 'Finalizar',
                      onConfirm: finalizeServiceOrder);
                }
              : () async => await routeAlreadyExecuted(
                  message:
                      'A ordem de serviço #${widget.order.code} já foi ${statusMap[widget.order.status]}.'),
        ),
        Bubble(
          title: 'Cancelar',
          iconColor: Colors.white,
          bubbleColor: color,
          icon: Icons.delete,
          titleStyle: const TextStyle(fontSize: 14, color: Colors.white),
          onPress: widget?.order?.status == 'open'
              ? () async {
                  setState(() {
                    _animationController.reverse();
                  });

                  await alertToFinalizeAndToCancelOrder(
                      title: 'Cancelamento O.S ${widget.order.code}',
                      placeholder:
                          'Descreva o motivo para o cancelamento (opcional)',
                      confirm: 'Confirmar',
                      onConfirm: cancelServiceOrder);
                }
              : () async => await routeAlreadyExecuted(
                  message:
                      'A ordem de serviço #${widget.order.code} já foi ${statusMap[widget.order.status]}.'),
        ),
      ],
      onPress: () {
        if (_animationController.isCompleted) {
          setState(() {
            _animationController.reverse();
          });
        } else {
          setState(() {
            _animationController.forward();
          });
        }
      },
      animation: _animation,
      backGroundColor: color,
      iconData: _animationController.isCompleted ? Icons.close : Icons.add,
      iconColor: const Color(0xffffffff),
    );
  }
}
