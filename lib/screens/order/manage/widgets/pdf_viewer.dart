import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:share_extend/share_extend.dart';

class PdfViewPage extends StatelessWidget {
  final String path;
  final String title;
  final String filename;

  PdfViewPage({
    @required this.path,
    @required this.title,
    @required this.filename,
  });

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
      appBar: AppBar(
        title: title != null ? Text(title) : null,
        actions: [
          IconButton(
            icon: const Icon(Icons.share),
            onPressed: () {
              ShareExtend.share(
                path,
                'file',
                sharePanelTitle: filename,
              );
            },
          ),
        ],
      ),
      path: path,
    );
  }
}
