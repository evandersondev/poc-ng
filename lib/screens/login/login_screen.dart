import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:poc_ng/assets/images.dart';
import 'package:poc_ng/screens/index.dart';
import 'package:poc_ng/shared/auth/auth_controller.dart';
import 'package:poc_ng/ui/molecules/button_icon.dart';
import 'package:poc_ng/utils/localstorage.dart';

class LoginScreen extends StatelessWidget {
  final authController = AuthController();

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final isDarkTheme = theme.brightness == Brightness.dark;
    final color = isDarkTheme ? theme.canvasColor : theme.colorScheme.secondary;
    final isLightBrightness = color.computeLuminance() > 0.5;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: color,
        elevation: 0,
        automaticallyImplyLeading: false,
        brightness: isLightBrightness ? Brightness.light : Brightness.dark,
      ),
      body: Column(
        children: [
          Container(
            color: theme.primaryColor,
            width: double.infinity,
            height: 140,
            child: Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: 70,
                child: Image.asset(
                  logoNameWhite,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 60, horizontal: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    // Column(
                    //   children: [
                    Text(
                      // 'Utilize sua conta para salvar seus dados se desejar',
                      'Clique no botão abaixo para começar a utilizar o Servicefy.',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(height: 35),
                    //     Container(
                    //       height: 170,
                    //       margin: EdgeInsets.only(top: 40),
                    //       child: Column(
                    //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //         children: [
                    //           ButtonIcon(
                    //             title: 'Continuar com Google',
                    //             backgroundColor: Color(0xFFDB4437),
                    //             leftIcon: FontAwesomeIcons.googlePlusG,
                    //             textStyle: TextStyle(color: Colors.white),
                    //             onPressed: () {},
                    //           ),
                    //           ButtonIcon(
                    //             title: 'Continuar com Facebook',
                    //             backgroundColor: Color(0xFF1977F3),
                    //             leftIcon: FontAwesomeIcons.facebookF,
                    //             textStyle: TextStyle(color: Colors.white),
                    //             onPressed: () {},
                    //           ),
                    //           ButtonIcon(
                    //             title: 'Continuar com E-mail',
                    //             backgroundColor: Colors.grey[700],
                    //             leftIcon: FontAwesomeIcons.solidEnvelope,
                    //             textStyle: TextStyle(color: Colors.white),
                    //             onPressed: () {},
                    //           ),
                    //         ],
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    Container(
                      margin: EdgeInsets.only(top: 15),
                      child: ButtonIcon(
                        // title: 'SICRONIZAR DEPOIS',
                        title: 'COMEÇAR',
                        textStyle: TextStyle(fontSize: 14, color: Colors.white),
                        rightIcon: Icons.arrow_forward_ios_outlined,
                        sizeIcon: 18,
                        onPressed: () =>
                            authController.setAuth(context, 'true'),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
