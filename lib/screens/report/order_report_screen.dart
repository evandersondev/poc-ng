import 'package:flutter/material.dart';
import 'package:poc_ng/database/order_product_relationship_database.dart';
import 'package:poc_ng/database/order_task_relationship_database.dart';
import 'package:poc_ng/database/service_order_database.dart';
import 'package:poc_ng/screens/product/product_screen.dart';
import 'package:poc_ng/screens/task/task_screen.dart';
import 'package:poc_ng/screens/order/manage/order_manage_screen.dart';
import 'package:poc_ng/shared/models/order_product_relationship_model.dart';
import 'package:poc_ng/shared/models/order_task_relationship_model.dart';
import 'package:poc_ng/shared/models/product_model.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';
import 'package:poc_ng/shared/models/task_model.dart';
import 'package:poc_ng/shared/themes/app_colors.dart';
import 'package:poc_ng/ui/molecules/button.dart';
import 'package:poc_ng/ui/molecules/custom_snackbar.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';

class OrderReportScreen extends StatefulWidget {
  final ServiceOrderModel order;
  final Function getProductsResources;
  final Function getTasksResources;
  final int pageIndex;

  OrderReportScreen({
    @required this.order,
    @required this.getProductsResources,
    @required this.getTasksResources,
    @required this.pageIndex,
  });

  @override
  _OrderReportScreenState createState() => _OrderReportScreenState();
}

class _OrderReportScreenState extends State<OrderReportScreen> {
  bool loading = false;
  bool confirmButton = false;
  List<String> tasksChecked = [];
  List<OrderProductRelationshipModel> products = [];
  List<OrderTaskRelationshipModel> tasks = [];

  final totalCostController = TextEditingController(text: '');

  @override
  void initState() {
    super.initState();

    initAsync();
  }

  void initAsync() async {
    final resourcesProducts = await widget.getProductsResources(widget.order);
    final resourcesTasks = await widget.getTasksResources(widget.order);
    setState(() {
      products = resourcesProducts;
      tasks = resourcesTasks;
    });
  }

  void setProducts(OrderProductRelationshipModel relationship) {
    if (products.length > 0) {
      setState(() {
        products = [...products, relationship];
      });
    } else {
      setState(() {
        products = [relationship];
      });
    }
  }

  void setTasks(OrderTaskRelationshipModel relationship) {
    if (tasks.length > 0) {
      setState(() {
        tasks = [...tasks, relationship];
      });
    } else {
      setState(() {
        tasks = [relationship];
      });
    }
  }

  void checkProducts(OrderProductRelationshipModel relationship, bool value) {
    var newCheckeds = products.map((item) => item.toJson()).toList();

    final updadeChecked = newCheckeds.map((item) {
      var check = item;
      if (item['id'] == relationship.id) {
        check = {
          ...check,
          'checked': value,
        };
      }
      return check;
    }).toList();

    final newRelationship = updadeChecked
        .map((item) => OrderProductRelationshipModel(
              id: item['id'],
              productAmount: item['productAmount'],
              checked: item['checked'],
              serviceOrder: widget.order,
              product: ProductModel.fromJson(item['product']),
            ))
        .toList();

    setState(() {
      products = newRelationship;
    });
  }

  void checkTasks(OrderTaskRelationshipModel relationship, bool value) {
    var newCheckeds = tasks.map((item) => item.toJson()).toList();
    final updadeChecked = newCheckeds.map((item) {
      var check = item;
      if (item['id'] == relationship.id) {
        check = {
          ...check,
          'checked': value,
        };
      }
      return check;
    }).toList();

    // print(tasks[0].serviceOrder);
    // print('PASSOU');

    final newRelationship = updadeChecked
        .map((item) => OrderTaskRelationshipModel(
              id: item['id'],
              checked: item['checked'],
              serviceOrder: widget.order,
              task: TaskModel.fromJson(item['task']),
            ))
        .toList();

    setState(() {
      tasks = newRelationship;
    });
  }

  void confirmReport() async {
    try {
      for (var i = 0; i < products.length; i++) {
        await OrderProductRelationshipDatabse.db
            .updateRelationship(products[i]);
      }

      for (var i = 0; i < tasks.length; i++) {
        await OrderTaskRelationshipDatabse.db.updateRelationship(tasks[i]);
      }

      final order = widget.order.toJson();
      final orderToUpdate = ServiceOrderModel.fromJson(
        {...order, 'report': totalCostController.text},
      );
      await ServiceOrderDatabase.db.updateOrder(orderToUpdate);

      final response =
          await ServiceOrderDatabase.db.showServiceOrder(orderToUpdate.id);

      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => OrderManageScreen(order: response),
        ),
      );

      ScaffoldMessenger.of(context).showSnackBar(
        snackbar(
            color: Colors.green,
            message: 'Reporte realizado na O.S. #${widget.order.code}.'),
      );
    } catch (error) {
      print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('O.S. ${widget.order.code} - Reporte'),
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: BackButton(
          color: AppColors.heading,
          onPressed: () => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (_) => OrderManageScreen(
                        order: widget.order,
                        pageIndex: widget.pageIndex,
                      ))),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(
                  'Cancelar',
                  style: TextStyle(color: theme.textTheme.bodyText1.color),
                )),
            SizedBox(width: 20),
            Button(
              title: 'Confirmar',
              enable: confirmButton,
              width: 120,
              onPressed: confirmReport,
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(15.0),
          child: !loading
              ? Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Previstos',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        height: 1,
                      ),
                    ),
                    Divider(
                      thickness: 1.5,
                      height: 15,
                      color: theme.textTheme.bodyText1.color,
                    ),
                    SizedBox(height: 30.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Produtos',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        Container(
                          height: 36,
                          child: Button(
                              title: 'Produto +',
                              width: 100,
                              backgroundColor: Colors.transparent,
                              textStyle: TextStyle(color: Colors.black),
                              onPressed: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => ProductScreen(
                                      setProducts: setProducts,
                                      report: true,
                                      order: widget.order,
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ],
                    ),
                    SizedBox(height: 15.0),
                    ...products.map((relationship) {
                      if (relationship != null) {
                        return Container(
                          height: 25,
                          padding:
                              const EdgeInsets.only(right: 8.0, left: 10.0),
                          margin: EdgeInsets.only(bottom: 12.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width - 100,
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      relationship.product.description,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      '${relationship.productAmount} UN',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                width: 30,
                                child: Checkbox(
                                  checkColor: Colors.white,
                                  // fillColor:,
                                  value: relationship.checked,
                                  onChanged: (bool value) =>
                                      checkProducts(relationship, value),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }).toList(),
                    SizedBox(height: 30.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Tarefas',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            )),
                        Container(
                          height: 36,
                          child: Button(
                              title: 'Tarefas +',
                              width: 100,
                              backgroundColor: Colors.transparent,
                              textStyle: TextStyle(color: Colors.black),
                              onPressed: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => TaskScreen(
                                      setTasks: setTasks,
                                      report: true,
                                      order: widget.order,
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ],
                    ),
                    SizedBox(height: 15.0),
                    ...tasks.map((relationship) {
                      return Container(
                        height: 25,
                        padding: const EdgeInsets.only(right: 14.0, left: 10.0),
                        margin: EdgeInsets.only(bottom: 12.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              relationship.task.description,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Checkbox(
                              checkColor: Colors.white,
                              value: relationship.checked,
                              onChanged: (bool value) =>
                                  checkTasks(relationship, value),
                            )
                          ],
                        ),
                      );
                    }).toList(),
                    SizedBox(height: 30.0),
                    Text(
                      'Custo Total da O.S.',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        height: 1,
                      ),
                    ),
                    Divider(
                      thickness: 1.5,
                      height: 15,
                      color: theme.textTheme.bodyText1.color,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 1.5,
                          child: TextField(
                            controller: totalCostController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              hintText: 'Valor (R\$)',
                            ),
                            inputFormatters: [
                              CurrencyTextInputFormatter(
                                locale: 'pt_br',
                                decimalDigits: 2,
                                symbol: 'R\$', // or to remove symbol set ''.
                              )
                            ],
                            onChanged: (value) {
                              if (value.isEmpty) {
                                setState(() {
                                  confirmButton = false;
                                });
                              } else {
                                setState(() {
                                  confirmButton = true;
                                });
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              : Center(child: CircularProgressIndicator()),
        ),
      ),
    );
  }
}
