import 'package:flutter/material.dart';
import 'package:poc_ng/assets/icons/selection.dart';
import 'package:poc_ng/screens/home/widgets/dashboard.dart';
import 'package:poc_ng/screens/home/widgets/history_updateds.dart';
import 'package:poc_ng/screens/home/widgets/menu/menu.dart';
import 'package:poc_ng/screens/order/create/order_create_screen.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';
import 'package:poc_ng/ui/molecules/button_icon.dart';
import 'package:poc_ng/database/service_order_database.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool loading = false;
  String ordersOpened;
  String ordersFinalized;
  List<ServiceOrderModel> serviceOrders;

  void initState() {
    super.initState();

    initAsync();
  }

  void initAsync() async {
    setState(() {
      loading = true;
    });

    final opened = await ServiceOrderDatabase.db.getCountOrdersOpened();
    final closed = await ServiceOrderDatabase.db.getCountOrdersClosed();
    final orders = await ServiceOrderDatabase.db.getFiveLastOrder();

    setState(() {
      ordersOpened = opened.toString();
      ordersFinalized = closed.toString();
      serviceOrders = orders;
      loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Olá, bom dia',
          style: TextStyle(
            color: theme.textTheme.bodyText1.color,
          ),
        ),
        automaticallyImplyLeading: false,
        actions: [
          Menu(),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 15, left: 15, right: 15),
        child: !loading
            ? Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Inicio',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                      SizedBox(
                        height: 15,
                      ),
                      ButtonIcon(
                          title: 'Nova Ordem de Serviço',
                          leftIcon: Icomoon.clipboard,
                          onPressed: () {
                            Navigator.pushReplacement(context,
                                MaterialPageRoute(builder: (_) {
                              return OrderCreateScreen(pageIndex: 0);
                            }));
                          }),
                      Dashboard(
                        ordersFinalized: ordersFinalized,
                        ordersOpened: ordersOpened,
                      ),
                      HistoryUpdateds(serviceOrders: serviceOrders),
                    ],
                  ),
                ],
              )
            : Center(child: CircularProgressIndicator()),
      ),
    );
  }
}
