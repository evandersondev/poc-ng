import 'package:flutter/material.dart';
import 'package:number_animation/number_animation.dart';

class Dashboard extends StatelessWidget {
  Dashboard({@required this.ordersFinalized, @required this.ordersOpened});

  final padding = 30;
  final String ordersOpened;
  final String ordersFinalized;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '  Pedentes',
                  style: TextStyle(fontWeight: FontWeight.bold, height: 2.5),
                ),
                Card(
                    elevation: 3,
                    shadowColor: theme.cardColor,
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: (MediaQuery.of(context).size.width / 2) - padding,
                      width: (MediaQuery.of(context).size.width / 2) - padding,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Icon(
                                Icons.circle_rounded,
                                color: Colors.red,
                                size: 26,
                              )
                            ]),
                            NumberAnimation(
                              isInt: true,
                              start: 0,
                              duration: Duration(milliseconds: 500),
                              end: int.tryParse(ordersOpened) ?? 0,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 48),
                            ),
                          ]),
                    )),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '  Finalizadas',
                  style: TextStyle(fontWeight: FontWeight.bold, height: 2.5),
                ),
                Card(
                    elevation: 3,
                    shadowColor: theme.cardColor,
                    child: Container(
                      padding: EdgeInsets.all(15),
                      height: (MediaQuery.of(context).size.width / 2) - padding,
                      width: (MediaQuery.of(context).size.width / 2) - padding,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Icon(
                                Icons.circle_rounded,
                                color: Colors.green,
                                size: 26,
                              )
                            ]),
                            NumberAnimation(
                              isInt: true,
                              start: 0,
                              duration: Duration(milliseconds: 500),
                              end: int.tryParse(ordersFinalized) ?? 0,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 48),
                            ),
                          ]),
                    )),
              ],
            ),
          ],
        )
      ],
    );
  }
}
