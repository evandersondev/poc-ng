import 'package:animate_icons/animate_icons.dart';
import 'package:flutter/material.dart';
import 'package:poc_ng/shared/themes/app_theme_controller.dart';

class Menu extends StatelessWidget {
  final controller = AnimateIconController();

  final themeController = AppThemeController.instance;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(themeController.isDark ? 'Modo Ligth' : 'Modo Dark',
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 16,
              color: themeController.isDark ? Colors.white : Colors.black87,
            )),
        AnimateIcons(
          startIcon: Icons.brightness_2_rounded,
          endIcon: Icons.brightness_7_rounded,
          size: 22,
          controller: controller,
          onStartIconPress: () {
            themeController.changeTheme();
            return true;
          },
          onEndIconPress: () {
            themeController.changeTheme();
            return true;
          },
          duration: Duration(milliseconds: 1000),
          startIconColor: Colors.black87,
          endIconColor: Colors.yellow[700],
        ),
      ],
    );
  }
}
