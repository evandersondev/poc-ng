import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:poc_ng/screens/order/manage/order_manage_screen.dart';
import 'package:animate_do/animate_do.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';

final Map<String, List> stylesCardOrder = {
  'open': [Icons.circle_rounded, Colors.red],
  'closed': [Icons.circle_rounded, Colors.green],
  'canceled': [Icons.remove_circle_outline, Colors.grey],
};

class HistoryUpdateds extends StatelessWidget {
  final List<ServiceOrderModel> serviceOrders;

  HistoryUpdateds({@required this.serviceOrders});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      margin: EdgeInsets.only(top: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 2),
          Text('  Ultimas atualizações',
              style: TextStyle(fontWeight: FontWeight.bold)),
          Divider(),
          SizedBox(height: 4),
          Container(
            height: MediaQuery.of(context).size.height * 0.347,
            child: serviceOrders != null && serviceOrders.length > 0
                ? Center(
                    child: ListView(
                      children: [
                        ...serviceOrders.map((item) {
                          return BounceInUp(
                              child: InkWell(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => OrderManageScreen(
                                      order: item, pageIndex: 0),
                                ),
                              );
                            },
                            child: Card(
                              elevation: 5,
                              shadowColor: theme.cardColor,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 12),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('#${item.code}',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                        SizedBox(height: 8),
                                        Text(
                                            DateFormat('d/MM/y - H:mm', 'pt-BR')
                                                .format(DateTime.parse(
                                                    item.updatedAt)),
                                            style:
                                                TextStyle(color: Colors.grey)),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Icon(stylesCardOrder[item.status][0],
                                            size: 20,
                                            color: stylesCardOrder[item.status]
                                                [1])
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ));
                        }).toList(),
                      ],
                    ),
                  )
                : Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'Não há nenhuma atualização registrada no momento, crie uma nova ordem de serviço para começar visualizar as atualizações.'),
                  ),
          ),
        ],
      ),
    );
  }
}
