import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:poc_ng/database/database.dart';
import 'package:poc_ng/ui/colors.dart';
import 'package:poc_ng/ui/hollow_core_defaults.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'assets/images.dart';
import 'screens/splash/splash_screen.dart';
import 'shared/themes/app_theme_controller.dart';

class PocApp extends StatefulWidget {
  @override
  _PocAppState createState() => _PocAppState();
}

class _PocAppState extends State<PocApp> {
  String initialRoute;
  final themeController = AppThemeController.instance;

  void createDatabase() async {
    await DatabaseProvider.db.initDB();
  }

  void initState() {
    super.initState();

    createDatabase();
  }

  Widget _buildMaterialApp() {
    final lightTheme = ThemeData(
      brightness: Brightness.light,
      primarySwatch: Colors.yellow,
    );

    final darkTheme = ThemeData(
      primarySwatch: Colors.red,
      brightness: Brightness.dark,
      toggleableActiveColor: Colors.red,
    );

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));

    return AnimatedBuilder(
      animation: themeController,
      builder: (context, child) {
        return MaterialApp(
          title: 'Servicefy',
          debugShowCheckedModeBanner: false,
          themeMode: ThemeMode.light,
          theme: ThemeData(
            primarySwatch: NGColors.darkBlue,
            brightness:
                themeController.isDark ? Brightness.dark : Brightness.light,
            primaryColorBrightness: Brightness.light,
            primaryTextTheme: lightTheme.textTheme,
            textTheme: TextTheme(
              headline6: TextStyle(
                color: themeController.isDark ? Colors.white : Colors.black,
              ),
            ),
            appBarTheme: lightTheme.appBarTheme.copyWith(
              brightness:
                  themeController.isDark ? Brightness.dark : Brightness.light,
              color: themeController.isDark
                  ? Theme.of(context).appBarTheme.color
                  : Theme.of(context).bottomAppBarColor,
              textTheme: Theme.of(context).textTheme,
              iconTheme: Theme.of(context).iconTheme.copyWith(
                    color: themeController.isDark
                        ? Colors.white
                        : Theme.of(context).textTheme.bodyText1.color,
                  ),
            ),
            visualDensity: VisualDensity.adaptivePlatformDensity,
            chipTheme: lightTheme.chipTheme.copyWith(
              secondarySelectedColor: NGColors.darkBlue,
              secondaryLabelStyle: lightTheme.chipTheme.secondaryLabelStyle
                  .copyWith(color: Colors.white),
            ),
          ),
          darkTheme: darkTheme.copyWith(
            chipTheme: darkTheme.chipTheme.copyWith(
              secondarySelectedColor: Colors.white,
            ),
          ),
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            DefaultCupertinoLocalizations.delegate,
          ],
          supportedLocales: const [Locale('pt', 'BR')],
          home: Scaffold(
            resizeToAvoidBottomInset: false,
            body: SplashScreen(),
          ),
        );
      },
    );
  }

  Widget build(BuildContext context) {
    return HollowCoreDefaults(
      listEmptyImage: SvgPicture.asset(noDataPlaceholder),
      listEmptyText: const Text('Nenhum dado foi encontrado'),
      errorImage: SvgPicture.asset(errorPlaceholder),
      errorText: const Text('Não foi possível carregar os dados'),
      child: _buildMaterialApp(),
    );
  }
}
