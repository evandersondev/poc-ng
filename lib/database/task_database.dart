import 'package:poc_ng/database/database.dart';
import 'package:poc_ng/shared/models/task_model.dart';

class TaskDataBase {
  TaskDataBase();
  static TaskDataBase db = TaskDataBase();

  final String tableName = 'tasks';

  Future insertTask(TaskModel task) async {
    final db = await DatabaseProvider.db.database;
    return await db.insert(tableName, task.toJson());
  }

  Future<List<TaskModel>> getAllTasks() async {
    final db = await DatabaseProvider.db.database;
    final res = await db.query(tableName);
    final task = res.map((item) => TaskModel.fromJson(item)).toList();
    return task;
  }
}
