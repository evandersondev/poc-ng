import 'package:poc_ng/database/database.dart';
import 'dart:math';
import 'package:intl/intl.dart';
import 'package:poc_ng/shared/models/equipament_model.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';

class ServiceOrderDatabase {
  ServiceOrderDatabase();
  static ServiceOrderDatabase db = ServiceOrderDatabase();

  String tableName = 'service_orders';

  Future<ServiceOrderModel> createOrder(ServiceOrderModel order) async {
    final formatterCode = NumberFormat('000000');

    final count = await getCountOrders();
    final db = await DatabaseProvider.db.database;

    if (count == 0) {
      final newOrder = {
        'id': order.id,
        'code': formatterCode.format(int.parse(order.code)),
        'description': order.description,
        'localization': order.localization,
        'equipament_id': order?.equipament?.id ?? null,
        'expectedStart': order.expectedStart,
        'doneDate': order.doneDate,
        'status': 'open',
        'created_at': '${DateTime.now()}',
        'updated_at': '${DateTime.now()}',
      };

      await db.insert(tableName, newOrder);
      return await showServiceOrder(order.id);
    } else {
      final lastCode = await ServiceOrderDatabase.db.getLastCodeByOrders();

      final newOrder = {
        'id': order.id,
        'code': formatterCode.format((lastCode + int.parse(order.code))),
        'description': order.description,
        'localization': order.localization,
        'equipament_id': order?.equipament?.id ?? null,
        'expectedStart': order.expectedStart,
        'doneDate': order.doneDate,
        'status': 'open',
        'created_at': '${DateTime.now()}',
        'updated_at': '${DateTime.now()}',
      };

      await db.insert(tableName, newOrder);
      return await showServiceOrder(order.id);
    }
  }

  Future<List<ServiceOrderModel>> getAllServiceOrder() async {
    final db = await DatabaseProvider.db.database;

    final res = await db.rawQuery('''
      SELECT
        service_orders.id AS id,
        service_orders.code AS code,
        service_orders.description AS description,
        service_orders.localization AS localization,
        service_orders.expectedStart AS expectedStart,
        service_orders.doneDate AS doneDate,
        service_orders.status AS status,
        service_orders.details AS details,
        service_orders.reason AS reason,
        service_orders.report AS report,
        service_orders.created_at AS createdAt,
        service_orders.updated_at AS updatedAt,
        service_orders.equipament_id AS equipamentId,
        equipaments.description AS equipamentDescription,
        equipaments.code AS equipamentCode,
        equipaments.localization AS equipamentLocalization
      FROM 
        $tableName
      LEFT JOIN equipaments
      ON equipaments.id = service_orders.equipament_id
      ORDER BY service_orders.created_at DESC
    ''');

    return res
        .map((item) => ServiceOrderModel(
            id: item['id'],
            code: item['code'],
            description: item['description'],
            localization: item['localization'],
            expectedStart: item['expectedStart'],
            doneDate: item['doneDate'],
            status: item['status'],
            details: item['details'],
            reason: item['reason'],
            report: item['report'],
            createdAt: item['createdAt'],
            updatedAt: item['updatedAt'],
            equipament: EquipamentModel(
                id: item['equipamentId'],
                code: item['equipamentCode'],
                description: item['equipamentDescription'],
                localization: item['equipamentLocalization'])))
        .toList();
  }

  Future<int> getCountOrders() async {
    final db = await DatabaseProvider.db.database;
    final res = await db.query(tableName);
    final count = res.length;
    return count;
  }

  Future<int> getLastCodeByOrders() async {
    final db = await DatabaseProvider.db.database;
    final res = await db.query(tableName);
    final list = res.map((item) => int.parse(item['code'].toString())).toList();
    return list.reduce(max);
  }

  Future<int> updateOrder(ServiceOrderModel order) async {
    final db = await DatabaseProvider.db.database;

    final row = {
      'id': order.id,
      'code': order.code,
      'description': order.description,
      'localization': order.localization,
      'equipament_id': order?.equipament?.id ?? '',
      'expectedStart': order.expectedStart,
      'doneDate': order.doneDate,
      'status': order.status,
      'details': order.details,
      'reason': order.reason,
      'report': order.report,
      'updated_at': '${DateTime.now()}',
    };

    return await db
        .update(tableName, row, where: 'id = ?', whereArgs: [order.id]);
  }

  Future<ServiceOrderModel> showServiceOrder(String id) async {
    final db = await DatabaseProvider.db.database;

    final res = await db.rawQuery('''
      SELECT 
        service_orders.id as id,
        service_orders.code as code,
        service_orders.description as description,
        service_orders.localization as localization,
        service_orders.expectedStart as expectedStart,
        service_orders.status AS status,
        service_orders.details AS details,
        service_orders.reason AS reason,
        service_orders.report AS report,
        service_orders.created_at AS createdAt,
        service_orders.updated_at AS updatedAt,
        service_orders.equipament_id as equipamentId,
        equipaments.description as equipamentDescription,
        equipaments.code as equipamentCode,
        equipaments.localization as equipamentLocalization
      FROM 
        $tableName
      LEFT JOIN equipaments
      ON equipaments.id = service_orders.equipament_id
      WHERE service_orders.id=?
    ''', [id]);

    return ServiceOrderModel(
      id: res[0]['id'],
      code: res[0]['code'],
      description: res[0]['description'],
      localization: res[0]['localization'],
      expectedStart: res[0]['expectedStart'],
      status: res[0]['status'],
      details: res[0]['details'],
      reason: res[0]['reason'],
      report: res[0]['report'],
      createdAt: res[0]['createdAt'],
      updatedAt: res[0]['updatedAt'],
      equipament: res[0]['equipamentId'] != null
          ? EquipamentModel(
              id: res[0]['equipamentId'],
              code: res[0]['equipamentCode'],
              description: res[0]['equipamentDescription'],
              localization: res[0]['equipamentLocalization'],
            )
          : null,
    );
  }

  Future<int> getCountOrdersOpened() async {
    final db = await DatabaseProvider.db.database;
    final res =
        await db.query(tableName, where: 'status = ?', whereArgs: ['open']);
    return res.length;
  }

  Future<int> getCountOrdersClosed() async {
    final db = await DatabaseProvider.db.database;
    final res =
        await db.query(tableName, where: 'status = ?', whereArgs: ['closed']);
    return res.length;
  }

  Future<List<ServiceOrderModel>> getFiveLastOrder() async {
    final db = await DatabaseProvider.db.database;
    final res = await db.rawQuery('''
      SELECT
        service_orders.id AS id,
        service_orders.code AS code,
        service_orders.description AS description,
        service_orders.localization AS localization,
        service_orders.expectedStart AS expectedStart,
        service_orders.doneDate AS doneDate,
        service_orders.status AS status,
        service_orders.created_at AS createdAt,
        service_orders.updated_at AS updatedAt,
        service_orders.details AS details,
        service_orders.reason AS reason,
        service_orders.report AS report,
        service_orders.equipament_id AS equipamentId,
        equipaments.description AS equipamentDescription,
        equipaments.code AS equipamentCode,
        equipaments.localization AS equipamentLocalization
      FROM 
        $tableName
      LEFT JOIN equipaments
      ON equipaments.id = service_orders.equipament_id
      ORDER BY service_orders.updated_at DESC LIMIT 5
    ''');

    return res
        .map((item) => ServiceOrderModel(
            id: item['id'],
            code: item['code'],
            description: item['description'],
            localization: item['localization'],
            expectedStart: item['expectedStart'],
            doneDate: item['doneDate'],
            status: item['status'],
            details: item['details'],
            reason: item['reason'],
            report: item['report'],
            createdAt: item['createdAt'],
            updatedAt: item['updatedAt'],
            equipament: EquipamentModel(
                id: item['equipamentId'],
                code: item['equipamentCode'],
                description: item['equipamentDescription'],
                localization: item['equipamentLocalization'])))
        .toList();
  }
}
