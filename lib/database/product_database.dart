import 'package:poc_ng/database/database.dart';
import 'package:poc_ng/shared/models/product_model.dart';

class ProductDataBase {
  ProductDataBase();
  static ProductDataBase db = ProductDataBase();

  final String tableName = 'products';

  Future insertProduct(ProductModel product) async {
    final db = await DatabaseProvider.db.database;
    return await db.insert(tableName, product.toJson());
  }

  Future<List<ProductModel>> getAllProducts() async {
    final db = await DatabaseProvider.db.database;
    final res = await db.query(tableName);
    final product = res.map((item) => ProductModel.fromJson(item)).toList();
    return product;
  }
}
