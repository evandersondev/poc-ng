import 'dart:math';

import 'package:poc_ng/database/database.dart';
import 'package:intl/intl.dart';
import 'package:poc_ng/shared/models/equipament_model.dart';

class EquipamentDatabase {
  EquipamentDatabase();
  static EquipamentDatabase db = EquipamentDatabase();

  final String tableName = 'equipaments';

  Future<EquipamentModel> insertEquipament(EquipamentModel equipament) async {
    final formatterCode = NumberFormat('000000');

    final count = await getCountEquipaments();
    final db = await DatabaseProvider.db.database;

    if (count == 0) {
      final newEquipament = EquipamentModel(
        id: equipament.id,
        code: formatterCode.format(int.parse(equipament.code)),
        description: equipament.description,
        localization: equipament.localization,
      );

      await db.insert(tableName, newEquipament.toJson());

      return newEquipament;
    } else {
      final lastCode = await EquipamentDatabase.db.getLastCodeByEquipaments();

      final newEquipament = EquipamentModel(
        id: equipament.id,
        code: formatterCode.format((lastCode + int.parse(equipament.code))),
        description: equipament.description,
        localization: equipament.localization,
      );

      await db.insert(tableName, newEquipament.toJson());
      return newEquipament;
    }
  }

  Future<int> getCountEquipaments() async {
    final db = await DatabaseProvider.db.database;
    final res = await db.query(tableName);
    final count = res.length;
    return count;
  }

  Future<int> getLastCodeByEquipaments() async {
    final db = await DatabaseProvider.db.database;
    final res = await db.query(tableName);
    final list = res.map((item) => int.parse(item['code'].toString())).toList();
    return list.reduce(max);
  }

  Future<List<Map<String, dynamic>>> listAllEquipaments() async {
    final db = await DatabaseProvider.db.database;
    final res = await db.query(tableName);
    return res.toList();
  }

  Future<EquipamentModel> showEquipament(String id) async {
    final db = await DatabaseProvider.db.database;
    final res = await db.query(tableName, where: 'id = ?', whereArgs: [id]);
    return EquipamentModel.fromJson(res[0]);
  }
}
