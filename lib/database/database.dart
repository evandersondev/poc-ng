import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  DatabaseProvider();
  static final DatabaseProvider db = DatabaseProvider();
  static Database _database;

  final String id = 'id';
  final String code = 'code';
  final String description = 'description';
  final String equipamentId = 'equipament_id';
  final String expectedStart = 'expectedStart';
  final String doneDate = 'doneDate';
  final String localization = 'localization';
  final String status = 'status';
  final String details = 'details';
  final String reason = 'reason';
  final String report = 'report';
  final String createdAt = 'created_at';
  final String updatedAt = 'updated_at';
  final String serviceOrderId = 'service_order_id';
  final String productId = 'product_id';
  final String productAmount = 'product_amount';
  final String taskId = 'task_id';
  final String checked = 'checked';

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();
    return _database;
  }

  initDB() async {
    final directory = await getDatabasesPath();
    final path = join(directory, 'poc_ng.db');

    return await openDatabase(path, version: 1, onCreate: (db, version) async {
      await db.execute(
        'CREATE TABLE IF NOT EXISTS service_orders ('
        '$id TEXT PRIMARY KEY,'
        '$code TEXT,'
        '$description TEXT,'
        '$localization TEXT,'
        '$equipamentId TEXT,'
        '$expectedStart TEXT,'
        '$doneDate TEXT,'
        '$status TEXT,'
        '$details TEXT,'
        '$reason TEXT,'
        '$report TEXT,'
        '$createdAt TEXT DEFAULT CURRENT_TIMESTAMP,'
        '$updatedAt TEXT DEFAULT CURRENT_TIMESTAMP)',
      );

      await db.execute(
        'CREATE TABLE IF NOT EXISTS equipaments ('
        '$id TEXT PRIMARY KEY,'
        '$code TEXT,'
        '$description TEXT,'
        '$localization TEXT,'
        '$createdAt TEXT DEFAULT CURRENT_TIMESTAMP,'
        '$updatedAt TEXT DEFAULT CURRENT_TIMESTAMP)',
      );

      await db.execute(
        'CREATE TABLE IF NOT EXISTS products ('
        '$id TEXT PRIMARY KEY,'
        '$description TEXT,'
        '$createdAt TEXT DEFAULT CURRENT_TIMESTAMP,'
        '$updatedAt TEXT DEFAULT CURRENT_TIMESTAMP)',
      );

      await db.execute(
        'CREATE TABLE IF NOT EXISTS tasks ('
        '$id TEXT PRIMARY KEY,'
        '$description TEXT,'
        '$createdAt TEXT DEFAULT CURRENT_TIMESTAMP,'
        '$updatedAt TEXT DEFAULT CURRENT_TIMESTAMP)',
      );

      await db.execute(
        'CREATE TABLE IF NOT EXISTS orders_products_relationship ('
        '$id TEXT PRIMARY KEY,'
        '$serviceOrderId TEXT,'
        '$productId TEXT,'
        '$productAmount TEXT,'
        '$checked TEXT,'
        '$createdAt TEXT DEFAULT CURRENT_TIMESTAMP,'
        '$updatedAt TEXT DEFAULT CURRENT_TIMESTAMP)',
      );

      await db.execute(
        'CREATE TABLE IF NOT EXISTS orders_tasks_relationship ('
        '$id TEXT PRIMARY KEY,'
        '$serviceOrderId TEXT,'
        '$taskId TEXT,'
        '$checked TEXT,'
        '$createdAt TEXT DEFAULT CURRENT_TIMESTAMP,'
        '$updatedAt TEXT DEFAULT CURRENT_TIMESTAMP)',
      );
    });
  }
}
