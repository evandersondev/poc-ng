import 'package:poc_ng/database/database.dart';
import 'package:poc_ng/shared/models/order_task_relationship_model.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';
import 'package:poc_ng/shared/models/task_model.dart';

extension BoolParsing on String {
  bool parseBool() {
    return this.toLowerCase() == 'true';
  }
}

class OrderTaskRelationshipDatabse {
  OrderTaskRelationshipDatabse();
  static OrderTaskRelationshipDatabse db = OrderTaskRelationshipDatabse();

  final String tableName = 'orders_tasks_relationship';

  Future insertRelationship(OrderTaskRelationshipModel relationship) async {
    final db = await DatabaseProvider.db.database;
    final orderTask = {
      'id': relationship.id,
      'service_order_id': relationship.serviceOrder.id,
      'task_id': relationship.task.id,
      'checked': '${relationship.checked}',
    };

    return await db.insert(tableName, orderTask);
  }

  Future<List<OrderTaskRelationshipModel>> getAllRelationships(
      ServiceOrderModel order) async {
    final db = await DatabaseProvider.db.database;

    final res = await db.rawQuery('''
      SELECT
        orders_tasks_relationship.id AS id,
        orders_tasks_relationship.checked AS checked,
        tasks.id AS taskId,
        tasks.description AS taskDescription
      FROM
        $tableName
      LEFT JOIN tasks
      ON tasks.id = orders_tasks_relationship.task_id
      WHERE orders_tasks_relationship.service_order_id=?
    ''', [order.id]);

    final relationship = res
        .map((item) => OrderTaskRelationshipModel(
              id: item['id'],
              serviceOrder: null,
              checked: item['checked'].toString().parseBool(),
              task: TaskModel.fromJson({
                'id': item['taskId'],
                'description': item['taskDescription'],
              }),
            ))
        .toList();
    return relationship;
  }

  Future updateRelationship(OrderTaskRelationshipModel relationship) async {
    final db = await DatabaseProvider.db.database;

    final row = {
      'id': relationship.id,
      'service_order_id': relationship.serviceOrder.id,
      'task_id': relationship.task.id,
      'checked': relationship.checked.toString(),
    };

    return await db.update(
      tableName,
      row,
      where: 'id = ?',
      whereArgs: [relationship.id],
    );
  }
}
