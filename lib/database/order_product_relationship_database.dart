import 'package:poc_ng/database/database.dart';
import 'package:poc_ng/shared/models/order_product_relationship_model.dart';
import 'package:poc_ng/shared/models/product_model.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';

extension BoolParsing on String {
  bool parseBool() {
    return this.toLowerCase() == 'true';
  }
}

class OrderProductRelationshipDatabse {
  OrderProductRelationshipDatabse();
  static OrderProductRelationshipDatabse db = OrderProductRelationshipDatabse();

  final String tableName = 'orders_products_relationship';

  Future insertRelationship(OrderProductRelationshipModel relationship) async {
    final db = await DatabaseProvider.db.database;
    final orderProduct = {
      'id': relationship.id,
      'service_order_id': relationship.serviceOrder.id,
      'product_id': relationship.product.id,
      'product_amount': relationship.productAmount.toString(),
      'checked': '${relationship.checked}',
    };

    return await db.insert(tableName, orderProduct);
  }

  Future<List<OrderProductRelationshipModel>> getAllRelationships(
      ServiceOrderModel order) async {
    final db = await DatabaseProvider.db.database;

    final res = await db.rawQuery('''
      SELECT
        orders_products_relationship.id AS id,
        orders_products_relationship.product_amount AS productAmount,
        orders_products_relationship.checked AS checked,
        products.id AS productId,
        products.description AS productDescription
      FROM
        $tableName
      LEFT JOIN products
      ON products.id = orders_products_relationship.product_id
      WHERE orders_products_relationship.service_order_id=?
    ''', [order.id]);

    final relationship = res
        .map((item) => OrderProductRelationshipModel(
              id: item['id'],
              serviceOrder: null,
              checked: item['checked'].toString().parseBool(),
              productAmount: int.parse(item['productAmount']),
              product: ProductModel.fromJson({
                'id': item['productId'],
                'description': item['productDescription'],
              }),
            ))
        .toList();
    return relationship;
  }

  Future<int> updateRelationship(
      OrderProductRelationshipModel relationship) async {
    final db = await DatabaseProvider.db.database;

    final row = {
      'id': relationship.id,
      'service_order_id': relationship.serviceOrder.id,
      'product_id': relationship.product.id,
      'product_amount': relationship.productAmount.toString(),
      'checked': relationship.checked.toString(),
    };

    return await db.update(
      tableName,
      row,
      where: 'id = ?',
      whereArgs: [relationship.id],
    );
  }
}
