const _imagesPath = 'lib/assets/images';

const logoTipo = '$_imagesPath/logotipo.png';
const logoName = '$_imagesPath/logoname.png';
const splashLogo = '$_imagesPath/splash_logo.png';
const logoNameWhite = '$_imagesPath/logoname-white.png';
const welcomeOne = '$_imagesPath/welcome_01.jpg';
const welcomeTwo = '$_imagesPath/welcome_02.jpg';
const noDataPlaceholder = '$_imagesPath/no-data-placeholder.svg';
const errorPlaceholder = '$_imagesPath/error-placeholder.svg';
