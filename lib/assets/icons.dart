const _iconsPath = 'lib/assets/icons';

const emailIcon = '$_iconsPath/email.png';
const facebookIcon = '$_iconsPath/facebook.png';
const googleIcon = '$_iconsPath/google.png';
