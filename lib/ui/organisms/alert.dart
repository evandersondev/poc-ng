import 'package:flutter/material.dart';

class Alert {
  static Future<void> showAlert({
    @required BuildContext context,
    @required String title,
    @required Widget content,
    @required List<Widget> actions,
  }) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return StatefulBuilder(builder: (context, setState) {
          return AlertDialog(
            elevation: 9999,
            title: Text(title, style: TextStyle(fontSize: 16)),
            content: content,
            actions: actions,
          );
        });
      },
    );
  }
}
