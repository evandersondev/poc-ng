import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:poc_ng/assets/icons/selection.dart';

class DatePicker extends StatefulWidget {
  final String title;
  final Function onChanged;
  final DateTime value;

  const DatePicker({
    @required this.title,
    @required this.onChanged,
    this.value,
  });
  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  String dateSelected;

  void getDate() {
    if (widget.value != null) {
      final day = DateFormat('dd/MM/y', 'pt-BR').format(widget.value);

      setState(() {
        dateSelected = day;
      });
    }
  }

  Future<DateTime> selectDate(BuildContext context) async => showDatePicker(
        context: context,
        initialDate: widget.value ?? DateTime.now(),
        firstDate: DateTime(1990),
        lastDate: DateTime(2100),
      );

  @override
  Widget build(BuildContext context) {
    getDate();
    final theme = Theme.of(context);

    return InkWell(
      child: Container(
        width: 180,
        // height: 45,
        padding: EdgeInsets.only(top: 0),
        decoration: BoxDecoration(
            border: Border.all(
                color: dateSelected == null ? Colors.grey : Colors.black)),
        child: ListTile(
          title: Container(
              child: Text(
            dateSelected == null ? '    /    /' : dateSelected,
            style: TextStyle(fontSize: 16),
          )),
          dense: true,
          contentPadding: EdgeInsets.zero,
          leading: Container(
              padding: EdgeInsets.only(left: 8),
              child: Icon(Icomoon.calendar, color: theme.primaryColor)),
          enabled: true,
          focusColor: theme.primaryColor,
        ),
      ),
      onTap: () async {
        final selectedDate = await selectDate(context);

        if (selectedDate == null) return;

        final date = DateTime(
          selectedDate.year,
          selectedDate.month,
          selectedDate.day,
        );

        final day = DateFormat('d/MM/y', 'pt-BR').format(date);

        setState(() {
          dateSelected = day;
        });

        await widget.onChanged(date);
      },
    );
  }
}
