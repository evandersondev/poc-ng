import 'package:flutter/material.dart';
import 'package:poc_ng/assets/icons/selection.dart';

class TimePicker extends StatefulWidget {
  final String title;
  final Function onChanged;
  final TimeOfDay value;

  const TimePicker({
    @required this.title,
    @required this.onChanged,
    this.value,
  });
  @override
  _TimePickerState createState() => _TimePickerState();
}

class _TimePickerState extends State<TimePicker> {
  String timeSelected;

  Future<TimeOfDay> selectTime(BuildContext context) async {
    final now = DateTime.now();
    return showTimePicker(
      context: context,
      initialTime:
          widget.value ?? TimeOfDay(hour: now.hour, minute: now.minute),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return InkWell(
      child: Container(
        width: 180,
        // height: 45,
        padding: EdgeInsets.only(top: 0),
        decoration: BoxDecoration(
            border: Border.all(
                color: timeSelected == null ? Colors.grey : Colors.black)),
        child: ListTile(
          title: Container(
              child: Text(
            timeSelected == null ? '00:00' : timeSelected,
            style: TextStyle(fontSize: 16),
          )),
          dense: true,
          contentPadding: EdgeInsets.zero,
          leading: Container(
              padding: EdgeInsets.only(left: 8),
              child: Icon(Icomoon.timer, color: theme.primaryColor)),
          enabled: true,
          focusColor: theme.primaryColor,
        ),
      ),
      onTap: () async {
        final selectedTime = await selectTime(context);

        if (selectedTime == null) return;

        final time = selectedTime.format(context);

        setState(() {
          timeSelected = time;
        });

        await widget.onChanged(selectedTime);
      },
    );
  }
}
