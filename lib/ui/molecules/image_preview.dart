import 'dart:io';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ImagePreview extends StatelessWidget {
  const ImagePreview(this.image, this._deleteImageDialog);

  final File image;
  final void Function(File) _deleteImageDialog;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.delete, color: Colors.black),
            onPressed: () {
              _deleteImageDialog(image);
            },
          )
        ],
      ),
      body: Container(
        color: Colors.black87,
        child: PhotoView(
          heroAttributes: const PhotoViewHeroAttributes(tag: 'image_preview'),
          imageProvider: FileImage(image),
        ),
      ),
    );
  }
}
