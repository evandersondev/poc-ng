import 'package:flutter/material.dart';

/// Render a standard entity card
/// with a optional sidebar color, default height and
/// onTap support
class EntityCard extends StatelessWidget {
  const EntityCard({
    @required this.child,
    this.onTap,
    this.color,
    this.height = 140.0,
  }) : assert(height != null);

  final GestureTapCallback onTap;
  final Widget child;
  final double height;

  /// When specified, the entity card will appear
  /// with a side bar border with this color
  final Color color;

  /// Render a more simple version of the entity
  /// card, meant for cards that have simple layout
  /// (for example, a 3 lines card)
  factory EntityCard.simple({
    @required List<Widget> children,
    Color color,
    VoidCallback onTap,
    double height = 140.0,
  }) {
    return EntityCard(
      onTap: onTap,
      color: color,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: children,
      ),
    );
  }

  Widget _renderContent(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 5),
        child: child,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(2.0),
        child: Container(
          height: height,
          decoration: color != null
              ? BoxDecoration(
                  border: Border(
                    left: BorderSide(width: 10.0, color: color),
                  ),
                )
              : null,
          child: _renderContent(context),
        ),
      ),
    );
  }
}
