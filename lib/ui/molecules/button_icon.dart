import 'package:flutter/material.dart';

class ButtonIcon extends StatelessWidget {
  final String title;
  final Color backgroundColor;
  final Function onPressed;
  final TextStyle textStyle;
  final double width;
  final IconData rightIcon;
  final IconData leftIcon;
  final double sizeIcon;

  const ButtonIcon({
    @required this.title,
    @required this.onPressed,
    this.backgroundColor,
    this.textStyle,
    this.width,
    this.rightIcon,
    this.leftIcon,
    this.sizeIcon,
  });

  Widget leading(context) {
    final theme = Theme.of(context);
    final isDarkTheme = theme.brightness == Brightness.dark;
    final color = isDarkTheme ? theme.textTheme.bodyText1.color : Colors.white;

    if (leftIcon != null) {
      return Container(
        child: Icon(leftIcon, size: sizeIcon, color: color),
      );
    } else {
      return SizedBox();
    }
  }

  Widget trailing(context) {
    final theme = Theme.of(context);
    final isDarkTheme = theme.brightness == Brightness.dark;
    final color = isDarkTheme ? theme.textTheme.bodyText1.color : Colors.white;

    if (rightIcon != null) {
      return Container(
        child: Icon(rightIcon, size: sizeIcon, color: color),
      );
    } else {
      return SizedBox();
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final isDarkTheme = theme.brightness == Brightness.dark;
    final color = isDarkTheme ? theme.textTheme.bodyText1.color : Colors.white;

    return Container(
      width: width ?? double.infinity,
      decoration: BoxDecoration(
        color: backgroundColor ?? theme.primaryColor,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(2),
      ),
      child: TextButton(
          onPressed: onPressed,
          child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                leading(context),
                Text(
                  title,
                  style: textStyle ??
                      TextStyle(
                        color: color,
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                ),
                trailing(context),
              ])),
    );
  }
}
