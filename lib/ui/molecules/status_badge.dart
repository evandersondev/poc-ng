import 'package:flutter/material.dart';

Map<String, List> statusMap = {
  'open': ['Aberta', Colors.red],
  'closed': ['Finalizada', Colors.green],
  'canceled': ['Cancelada', Colors.grey],
};

class StatusBadge extends StatelessWidget {
  final String status;

  StatusBadge({@required this.status});

  @override
  Widget build(BuildContext context) {
    return Text(
      statusMap[status][0],
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 12,
        color: statusMap[status][1],
      ),
    );
  }
}
