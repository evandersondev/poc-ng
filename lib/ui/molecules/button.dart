import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final String title;
  final Color backgroundColor;
  final Function onPressed;
  final TextStyle textStyle;
  final double width;
  final bool enable;

  const Button({
    @required this.title,
    @required this.onPressed,
    this.enable = true,
    this.backgroundColor,
    this.textStyle,
    this.width,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final isDarkTheme = theme.brightness == Brightness.dark;
    final color = isDarkTheme ? theme.canvasColor : theme.colorScheme.secondary;
    final isLightBrightness = color.computeLuminance() > 0.5;

    return Container(
      width: width ?? double.infinity,
      decoration: BoxDecoration(
        color: enable
            ? !isLightBrightness
                ? backgroundColor ?? theme.primaryColor
                : Colors.red
            : Colors.grey,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(2),
        border: backgroundColor == Colors.transparent
            ? Border.all(color: theme.textTheme.bodyText1.color, width: 1)
            : null,
      ),
      child: TextButton(
        onPressed: enable ? onPressed : null,
        child: Text(
          title,
          style: textStyle ??
              TextStyle(
                color: isLightBrightness ? theme.buttonColor : Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
        ),
      ),
    );
  }
}
