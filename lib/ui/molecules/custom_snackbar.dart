import 'package:flutter/material.dart';

SnackBar snackbar(
    {@required Color color, @required String message, SnackBarAction action}) {
  return SnackBar(
    action: action != null ? action : null,
    elevation: 6.0,
    backgroundColor: color,
    behavior: SnackBarBehavior.floating,
    padding: const EdgeInsets.symmetric(
      vertical: 8,
      horizontal: 14,
    ),
    content: Row(
      children: [
        Text(
          message,
          style: const TextStyle(
            fontSize: 14,
            color: Colors.white,
          ),
        ),
      ],
    ),
  );
}
