import 'package:flutter/material.dart';

/// This class provides a way to globally configure
/// some aspects of the hollow core, it works
/// like a theme and help us keep hollow core
/// clean of app specific codes while
/// still making it customizable on a global basis
@immutable
class HollowCoreDefaults extends InheritedWidget {
  const HollowCoreDefaults({
    @required Widget child,
    @required this.listEmptyImage,
    @required this.listEmptyText,
    @required this.errorImage,
    @required this.errorText,
  })  : assert(listEmptyImage != null),
        assert(listEmptyText != null),
        super(child: child);

  final Widget listEmptyImage;
  final Widget listEmptyText;
  final Widget errorImage;
  final Widget errorText;

  static HollowCoreDefaults of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<HollowCoreDefaults>();

  @override
  bool updateShouldNotify(HollowCoreDefaults oldWidget) =>
      listEmptyImage != oldWidget.listEmptyImage ||
      listEmptyText != oldWidget.listEmptyText ||
      errorImage != oldWidget.errorImage ||
      errorText != oldWidget.errorText;
}
