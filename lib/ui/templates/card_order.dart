import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:poc_ng/screens/order/manage/order_manage_screen.dart';
import 'package:poc_ng/shared/models/service_order_model.dart';
import 'package:poc_ng/ui/molecules/entity_card.dart';
import 'package:poc_ng/ui/molecules/status_badge.dart';

Map<String, Color> colorCard = {
  'open': Colors.red,
  'closed': Colors.green,
  'canceled': Colors.grey,
};

class CardOrder extends StatelessWidget {
  final String notInformated = 'Não informado';
  final ServiceOrderModel serviceOrder;
  final int pageIndex;

  CardOrder({@required this.serviceOrder, this.pageIndex});

  TextTheme _textTheme(BuildContext context) {
    return Theme.of(context).textTheme;
  }

  TextStyle _captionTheme(BuildContext context) {
    return _textTheme(context).caption.copyWith(fontWeight: FontWeight.w500);
  }

  Widget _renderTopLine(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            '#${serviceOrder.code}',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              decoration: serviceOrder.status == 'canceled'
                  ? TextDecoration.lineThrough
                  : TextDecoration.none,
            ),
          ),
          StatusBadge(status: serviceOrder.status),
        ],
      ),
    );
  }

  Widget _renderMiddleLine(BuildContext context) {
    final textTheme = _textTheme(context).bodyText1;

    return Expanded(
      child: Text(
        'Problema: ${serviceOrder.description != '' ? serviceOrder.description : notInformated}',
        style: textTheme,
      ),
    );
  }

  Widget _renderBottomLine(BuildContext context) {
    final textTheme = _captionTheme(context);
    final boldTextTheme = textTheme.copyWith(fontWeight: FontWeight.bold);

    return Container(
      height: 30,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Previsão: ',
                    style: boldTextTheme,
                  ),
                  Text(
                    DateFormat('d/MM/y - hh:mm', 'pt-BR')
                        .format(DateTime.parse(serviceOrder.expectedStart)),
                    style: textTheme,
                  )
                ],
              ),
            ],
          ),
          renderStatusDate(context),
        ],
      ),
    );
  }

  Widget renderStatusDate(context) {
    final textTheme = _captionTheme(context);
    final boldTextTheme = textTheme.copyWith(fontWeight: FontWeight.bold);

    if (serviceOrder.status == 'closed') {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'Realização: ',
            style: boldTextTheme,
          ),
          Text(
            DateFormat('dd/MM/y - hh:mm', 'pt-BR')
                .format(DateTime.parse(serviceOrder.doneDate)),
            style: textTheme,
          )
        ],
      );
    } else if (serviceOrder.status == 'canceled') {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'Cancelamento: ',
            style: boldTextTheme,
          ),
          Text(
            DateFormat('d/MM/y - hh:mm', 'pt-BR')
                .format(DateTime.parse(serviceOrder.updatedAt)),
            style: textTheme,
          )
        ],
      );
    } else {
      return Text('');
    }
  }

  @override
  Widget build(BuildContext context) {
    return EntityCard.simple(
      onTap: () => Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (_) => OrderManageScreen(
            order: serviceOrder,
            pageIndex: pageIndex,
          ),
        ),
      ),
      color: colorCard[serviceOrder.status],
      children: [
        _renderTopLine(context),
        _renderMiddleLine(context),
        _renderBottomLine(context),
      ],
    );
  }
}
