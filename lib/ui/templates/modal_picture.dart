import 'dart:io';

import 'package:flutter/material.dart';
import 'package:poc_ng/assets/icons/selection.dart';
import 'package:image_picker/image_picker.dart';

import '../../assets/icons/selection.dart';
import '../molecules/image_preview.dart';

class ModalPicturesGrid extends StatefulWidget {
  final bool enable;
  final List<File> images;
  final Function setImage;
  const ModalPicturesGrid(
      {@required this.images, this.setImage, this.enable: true});
  _ModalPicturesGrid createState() => _ModalPicturesGrid();
}

class _ModalPicturesGrid extends State<ModalPicturesGrid> {
  _ModalPicturesGrid({this.maxHeight = 255.0});

  final double maxHeight;

  @override
  Widget build(BuildContext context) {
    final picker = ImagePicker();

    Future getImage(int type) async {
      final pickedFile = await picker.getImage(
          source: type == 1 ? ImageSource.camera : ImageSource.gallery);

      setState(() {
        if (pickedFile != null) {
          var _image = File(pickedFile.path);
          widget.setImage(_image);
        } else {
          print('No image selected.');
        }
      });
    }

    final theme = Theme.of(context);

    final textSubtitle1 = theme.primaryTextTheme.subtitle1;
    Future<void> _showDialog() async {
      switch (await showDialog<int>(
          context: context,
          builder: (context) {
            return SimpleDialog(
              title: const Text('Selecione uma opção'),
              children: <Widget>[
                SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, 1);
                  },
                  child: DefaultTextStyle(
                      child: const Text('Tirar uma foto'),
                      style: textSubtitle1),
                ),
                SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, 2);
                  },
                  child: DefaultTextStyle(
                      child: const Text('Escolher da galeria'),
                      style: textSubtitle1),
                ),
              ],
            );
          })) {
        case 1:
          getImage(1);
          break;
        case 2:
          getImage(2);
          break;
      }
    }

    void _deleteImage(File imageToRemove) {
      setState(() {
        widget.images.removeWhere((image) => image == imageToRemove);
      });
      Navigator.of(context).pop();
    }

    void _deleteImageDialog(File image) async {
      switch (await showDialog<int>(
          context: context,
          builder: (context) {
            return SimpleDialog(
              title: const Text('Excluir essa foto?'),
              children: <Widget>[
                SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, 1);
                  },
                  child: DefaultTextStyle(
                      child: const Text('Sim'), style: textSubtitle1),
                ),
                SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, 2);
                  },
                  child: DefaultTextStyle(
                      child: const Text('Não'), style: textSubtitle1),
                ),
              ],
            );
          })) {
        case 1:
          _deleteImage(image);
          break;
        case 2:
          break;
      }
    }

    List<Widget> imagesPreview = widget.images
        .map((image) => InkWell(
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ImagePreview(image, _deleteImageDialog),
                  fullscreenDialog: true,
                ),
              ),
              child: Container(
                  width: 400,
                  margin: const EdgeInsets.only(left: 0.1),
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.black)),
                  child: Image(image: FileImage(image))),
            ))
        .toList();
    imagesPreview.add(InkWell(
      onTap: widget.enable ? _showDialog : null,
      child: Container(
          width: 400,
          padding: const EdgeInsets.fromLTRB(20.0, 40.0, 20.0, 40.0),
          margin: const EdgeInsets.only(left: 0.1),
          decoration: BoxDecoration(border: Border.all(color: Colors.black)),
          // ignore: prefer_const_literals_to_create_immutables
          child: Column(children: [
            const Icon(Icomoon.take_photo, size: 38),
          ])),
    ));

    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      curve: Curves.fastOutSlowIn,
      constraints: BoxConstraints(
        maxHeight: widget.images.length > 2 ? maxHeight : 128,
      ),
      child: ConstrainedBox(
        constraints: BoxConstraints(
            // maxHeight: maxHeight,
            ),
        child: GridView.count(
          crossAxisCount: 3,
          children: imagesPreview,
        ),
      ),
    );
  }
}
