import 'package:flutter/material.dart';

class NGColors {
  static const int _bluePrimaryValue = 0xFF0f1c31;

  static const MaterialColor darkBlue = MaterialColor(
    _bluePrimaryValue,
    <int, Color>{
      50: Color(0xFF7b828e),
      100: Color(0xFF6f7783),
      200: Color(0xFF57606f),
      300: Color(0xFF3f495a),
      400: Color(0xFF273346),
      500: Color(0xFF0f1c31),
      600: Color(0xFF0d192c),
      700: Color(0xFF0c1627),
      800: Color(0xFF0a1422),
      900: Color(0xFF09111d),
    },
  );
}
